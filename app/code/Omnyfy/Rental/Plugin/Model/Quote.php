<?php

namespace Omnyfy\Rental\Plugin\Model;

use SalesIgniter\Rental\Model\Product\Type\Sirent;

class Quote
{
    /**
     * @var \Omnyfy\Vendor\Model\Resource\Vendor
     */
    private $vendorResource;
    /**
     * @var \Omnyfy\Vendor\Model\Resource\Location\CollectionFactory
     */
    private $locationCollectionFactory;
    /**
     * @var \Omnyfy\Vendor\Model\Resource\Inventory\CollectionFactory
     */
    private $inventoryCollectionFactory;

    public function __construct(
        \Omnyfy\Vendor\Model\Resource\Vendor $vendorResource,
        \Omnyfy\Vendor\Model\Resource\Inventory\CollectionFactory $inventoryCollectionFactory,
        \Omnyfy\Vendor\Model\Resource\Location\CollectionFactory $locationCollectionFactory
    )
    {
        $this->vendorResource = $vendorResource;
        $this->locationCollectionFactory = $locationCollectionFactory;
        $this->inventoryCollectionFactory = $inventoryCollectionFactory;
    }

    public function aroundAddProduct(
        \Magento\Quote\Model\Quote $quote,
        callable $proceed,
        \Magento\Catalog\Model\Product $product,
        $request = null,
        $processMode = \Magento\Catalog\Model\Product\Type\AbstractType::PROCESS_MODE_FULL
    )
    {
        if ($product->getTypeId() == Sirent::TYPE_RENTAL) {
            $msg = "We don’t have as many \"" . $product->getName() . "\" as you requested";
            if ($vendorId = $this->vendorResource->getVendorIdByProductId($product->getId())) {
                $locationCollection = $this->locationCollectionFactory->create();
                $locationCollection = $locationCollection->addFieldToFilter('vendor_id', (int)$vendorId)->getData();
                if ($locationCollection[0] && isset($locationCollection[0]['entity_id'])) {
                    $locationId = (int)$locationCollection[0]['entity_id'];
                    $inventoryCollection = $this->inventoryCollectionFactory->create();
                    $inventoryCollection = $inventoryCollection->addFieldToFilter('location_id', $locationId)
                        ->addFieldToFilter('product_id', $product->getId())
                        ->getData();
                    if ($inventoryCollection[0]) {
                        if ($inventoryCollection[0]['qty'] > 0) {
                            return $proceed($product, $request, $processMode);
                        }
                    }
                }
            }
            throw new \Magento\Framework\Exception\LocalizedException(__($msg));
        } else {
            return $proceed($product, $request, $processMode);
        }
    }
}