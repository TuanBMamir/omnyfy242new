<?php

namespace Omnyfy\Rental\Plugin\Model\Report;

class Serialnumber
{
    /**
     * @var \Magento\Backend\Model\Session
     */
    private $session;
    /**
     * @var \Omnyfy\Vendor\Model\Resource\Vendor
     */
    protected $vendorResource;

    /**
     * Collection constructor.
     * @param \Magento\Backend\Model\Session $session
     * @param \Omnyfy\Vendor\Model\Resource\Vendor $vendorResource
     */
    public function __construct(
        \Magento\Backend\Model\Session $session,
        \Omnyfy\Vendor\Model\Resource\Vendor $vendorResource
    )
    {
        $this->session = $session;
        $this->vendorResource = $vendorResource;
    }

    /**
     * @param \SalesIgniter\Rental\Model\Report\Serialnumber $subject
     * @param $result
     * @return array
     */
    public function afterGetSerialsData(\SalesIgniter\Rental\Model\Report\Serialnumber $subject, $result){
        $vendorInfo = $this->session->getVendorInfo();

        if (!empty($vendorInfo) && isset($vendorInfo['is_vendor_admin'])) {
            $vendorId = $vendorInfo['vendor_id'];
            $productIds = $this->vendorResource->getProductIdsByVendorId($vendorId);
            $newResult = [];
            foreach ($result as $item){
                if (in_array($item['id'], $productIds)){
                    $newResult[] = $item;
                }
            }
            return $newResult;
        }

        return $result;
    }
}