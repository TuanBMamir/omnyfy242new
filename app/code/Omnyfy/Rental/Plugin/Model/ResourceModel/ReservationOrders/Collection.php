<?php


namespace Omnyfy\Rental\Plugin\Model\ResourceModel\ReservationOrders;


class Collection
{
    /**
     * @var \Magento\Backend\Model\Session
     */
    private $session;

    /**
     * Collection constructor.
     * @param \Magento\Backend\Model\Session $session
     */
    public function __construct(
        \Magento\Backend\Model\Session $session
    )
    {
        $this->session = $session;
    }

    public function beforeLoadWithFilter(\SalesIgniter\Rental\Model\ResourceModel\ReservationOrders\Collection $subject,
      $printQuery = false, $logQuery = false
    ){
        $vendorInfo = $this->session->getVendorInfo();
        if (!empty($vendorInfo) && isset($vendorInfo['is_vendor_admin'])) {
            $vendorId = $vendorInfo['vendor_id'];
            $subject->getSelect()->join(
                ['vp' => $subject->getTable("omnyfy_vendor_vendor_product")],
                'vp.product_id = main_table.product_id',
                ''
            )->where('vp.vendor_id = ?',$vendorId);
        }
        return [$printQuery, $logQuery];
    }
}
