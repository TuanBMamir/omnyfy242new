<?php

namespace Omnyfy\Rental\Plugin\Model;

use SalesIgniter\Rental\Model\Product\Type\Sirent;

class Product
{
    public function afterGetIsVirtual(
        \Magento\Catalog\Model\Product $product
    ) {
        if ($product->getTypeId() == Sirent::TYPE_RENTAL && $product->getWeight() == 0) {
            return true;
        }
        return false;
    }
}