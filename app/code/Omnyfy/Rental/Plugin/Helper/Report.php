<?php

namespace Omnyfy\Rental\Plugin\Helper;

use Magento\Backend\Model\Session;
use Magento\Framework\App\ResourceConnection;

class Report
{
    /**
     * @var Session
     */
    private $session;
    /**
     * @var \Omnyfy\Vendor\Model\Resource\Vendor
     */
    private $vendor;

    public function __construct(
        Session $session,
        \Omnyfy\Vendor\Model\Resource\Vendor $vendor
    )
    {
        $this->session = $session;
        $this->vendor = $vendor;
    }

    public function afterGetRentalOrders(\SalesIgniter\Rental\Helper\Report $subject, $result)
    {
        $vendorInfo = $this->session->getVendorInfo();
        if (!empty($vendorInfo) && isset($vendorInfo['is_vendor_admin'])) {
            $vendorResult = [];
            foreach ($result as $order) {
                if ($this->vendor->isOrderForVendor($order['order_id'], $vendorInfo['vendor_id'])) {
                    $vendorResult[] = $order;
                }
            }
            return $vendorResult;
        }
        return $result;
    }
}