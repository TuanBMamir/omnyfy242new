<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Omnyfy\Rental\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\ObjectManagerInterface;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use SalesIgniter\Rental\Model\Product\Type\Sirent;


class RentalProduct extends AbstractModifier
{
    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var array
     */
    protected $modifiers = [];

    /**
     * Object Manager
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @param LocatorInterface           $locator
     * @param ObjectManagerInterface     $objectManager
     * @param ProductRepositoryInterface $productRepository
     * @param array                      $modifiers
     */
    public function __construct(
        \SalesIgniter\Rental\Helper\Data $rentalHelper,
        LocatorInterface $locator,
        ObjectManagerInterface $objectManager,
        ProductRepositoryInterface $productRepository,
        array $modifiers = []
    ) {
        $this->rentalHelper = $rentalHelper;
        $this->locator = $locator;
        $this->objectManager = $objectManager;
        $this->productRepository = $productRepository;
        $this->modifiers = $modifiers;
    }


    public function modifyMeta(array $meta)
    {
        if ($this->locator->getProduct()->getTypeId() === Sirent::TYPE_RENTAL) {
            unset($meta['product-details']['children']['quantity_and_stock_status_qty']);
            unset($meta['product-details']['children']['container_weight']);

        }
        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        if ($this->locator->getProduct()->getTypeId() === Sirent::TYPE_RENTAL) {
            $productId = $this->locator->getProduct()->getId();
            $data[$productId]['product']['stock_data']['manage_stock'] = 0;
            $data[$productId]['product']['quantity_and_stock_status']['is_in_stock'] = 0;
            $data[$productId]['product']['stock_data']['use_config_manage_stock'] = 0;
            $data[$productId]['product']['weight'] = 0;
        }
        return $data;
    }
}