<?php

namespace Omnyfy\Rental\Observer;

use Magento\Framework\Exception\LocalizedException;
use SalesIgniter\Rental\Model\Product\Type\Sirent;

class QuoteItemCheckLocationId implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Omnyfy\Vendor\Helper\Extra
     */
    protected $_extraHelper;

    /**
     * @var \Omnyfy\Vendor\Model\Resource\Inventory
     */
    protected $inventory;

    /**
     * QuoteItemCheckLocationId constructor.
     * @param \Omnyfy\Vendor\Helper\Extra $extraHelper
     * @param \Omnyfy\Vendor\Model\Resource\Inventory $inventory
     */
    public function __construct(
        \Omnyfy\Vendor\Helper\Extra $extraHelper,
        \Omnyfy\Vendor\Model\Resource\Inventory $inventory
    )
    {
        $this->_extraHelper = $extraHelper;
        $this->inventory = $inventory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws LocalizedException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quoteItem = $observer->getEvent()->getItem();
        $product = $quoteItem->getProduct();

        if ($product->getTypeId() == Sirent::TYPE_RENTAL || $quoteItem->getVendorId()) {
            $message = __('Something wrong with location id of %1', $product->getName());
            if ($quoteItem->hasLocationId() && $quoteItem->hasVendorId()) {
                return;
            }else{
                $locationIds = $this->inventory->loadQtysByProductIds($product->getId(), $quoteItem->getStore()->getWebsiteId(),$quoteItem->getVendorId());
                $locationId = '';
                foreach($locationIds as $item) {
                    foreach ($item as $key => $subItem){
                        $locationId = $key;
                        $qty = $subItem[$key];
                    }
                }
                if ($locationId && $qty > 0){
                    $quoteItem->setData('location_id', $locationId);
                    $this->_extraHelper->updateAddressItemLocationId($quoteItem->getId(), $locationId);
                }
                return;
            }
            throw new LocalizedException(__($message));
        }
        return;
    }
}