<?php
namespace AHT\PreLogin\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CheckLoginObserver implements ObserverInterface
{

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;


    public function __construct(
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_customerSession = $customerSession;
    }

    public function execute(Observer $observer)
    {
        $actionName = $observer->getRequest()->getFullActionName();
        $a = $observer->getRequest();
        $b = 1;

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/action-name.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($actionName);
    }
}