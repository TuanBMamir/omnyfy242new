<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace SalesIgniter\Rental\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\SampleData\Model\Dependency;
use Symfony\Component\Console\Input\ArrayInputFactory;
use Magento\Framework\Filesystem;
use Composer\Console\ApplicationFactory;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Command for remove Rental attributes from products
 */
class Uninstall extends Command
{
    const OPTION_NO_UPDATE = 'no-update';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Dependency
     */
    private $sampleDataDependency;

    /**
     * @var ArrayInputFactory
     * @deprecated 100.1.0
     */
    private $arrayInputFactory;

    /**
     * @var ApplicationFactory
     */
    private $applicationFactory;

    /**
     * @param Filesystem $filesystem
     * @param Dependency $sampleDataDependency
     * @param ArrayInputFactory $arrayInputFactory
     * @param ApplicationFactory $applicationFactory
     */
    public function __construct(
        Filesystem $filesystem,
        Dependency $sampleDataDependency,
        ArrayInputFactory $arrayInputFactory,
        ApplicationFactory $applicationFactory,
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context,
        SchemaSetupInterface $schemaSetup
    )
    {
        $this->filesystem = $filesystem;
        $this->sampleDataDependency = $sampleDataDependency;
        $this->arrayInputFactory = $arrayInputFactory;
        $this->applicationFactory = $applicationFactory;
        $this->setup = $setup;
        $this->context = $context;
        $this->schemaSetup = $schemaSetup;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('rental:uninstall')
            ->setDescription('Caution: Removes all rental attributes and tables from database and setup version');
//        $this->addOption(
//            self::OPTION_NO_UPDATE,
//            null,
//            InputOption::VALUE_NONE,
//            'Update composer.json without executing composer update'
//        );
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $dialog = $this->getHelperSet()->get('question');

        $helper = $this->getHelper('question');

        $question = new ConfirmationQuestion('Are you sure you want to remove all rental product attributes and tables? Data will be lost forever - only run this if you are uninstalling permanently',
            'false'
        );
        if (!$helper->ask($input, $output, $question)) {
            return;
        }

        $setup = $this->setup;
        $context = $this->context;
        /** @var \Magento\Catalog\Setup\CategorySetup $catalogSetup */
        $catalogSetup = $this->catalogSetupFactory->create(['setup' => $setup]);


        $attributes = ['sirent_use_times',
            'sirent_use_times_grid',
            'sirent_min_global',
            'sirent_min_number',
            'sirent_min_type',
            'sirent_max_global',
            'sirent_max_number',
            'sirent_max_type',
            'sirent_turnover_before_global',
            'sirent_turnover_before_number',
            'sirent_turnover_before_type',
            'sirent_turnover_after_global',
            'sirent_turnover_after_number',
            'sirent_turnover_after_type',
            'sirent_excluded_days_global',
            'sirent_excluded_days',
            'sirent_has_shipping',
            'sirent_future_limit',
            'sirent_inv_bydate_serialized',
            'sirent_deposit_global',
            'sirent_deposit',
            'sirent_damage_waiver_global',
            'sirent_damage_waiver',
            'sirent_pricingtype',
            'sirent_price',
            'sirent_bundle_price_type',
            'sirent_quantity',
            'sirent_rental_type',
            'sirent_serial_numbers_use',
            'sirent_serial_numbers',
            'sirent_hour_next_day',
            'sirent_store_open_time',
            'sirent_store_close_time',
            'sirent_store_open_monday',
            'sirent_store_close_monday',
            'sirent_store_open_tuesday',
            'sirent_store_close_tuesday',
            'sirent_store_open_wednesday',
            'sirent_store_close_wednesday',
            'sirent_store_open_thursday',
            'sirent_store_close_thursday',
            'sirent_store_open_friday',
            'sirent_store_close_friday',
            'sirent_store_open_saturday',
            'sirent_store_close_saturday',
            'sirent_store_open_sunday',
            'sirent_store_close_sunday',
        ];

        $output->writeln('<info>' . count($attributes) . ' Rental product attributes removed.' . '</info>');

        foreach ($attributes as $attribute) {
            $catalogSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, $attribute);
        }

        $tables = ['sirental_fixed_dates',
            'sirental_fixed_names',
            'sirental_inventory_grid',
            'sirental_payment_transaction',
            'sirental_price',
            'sirental_reservationorders',
            'sirental_return',
            'sirental_serialnumber_details'];

        $installer = $this->schemaSetup;
        foreach ($tables as $table) {
            $installer->getConnection()->dropTable($table);
            $output->writeln('<info>' . $table . ' table removed.' . '</info>');
        }

        return $this;

//        $sampleDataPackages = $this->sampleDataDependency->getSampleDataPackages();
//        if (!empty($sampleDataPackages)) {
//            $baseDir = $this->filesystem->getDirectoryRead(DirectoryList::ROOT)->getAbsolutePath();
//            $commonArgs = ['--working-dir' => $baseDir, '--no-interaction' => 1, '--no-progress' => 1];
//            if ($input->getOption(self::OPTION_NO_UPDATE)) {
//                $commonArgs['--no-update'] = 1;
//            }
//            $packages = array_keys($sampleDataPackages);
//            $arguments = array_merge(['command' => 'remove', 'packages' => $packages], $commonArgs);
//            $commandInput = new ArrayInput($arguments);

    }
}
