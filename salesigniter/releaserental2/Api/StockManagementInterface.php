<?php
/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 10/25/2016
 * Time: 2:30 PM
 */

namespace SalesIgniter\Rental\Api;

interface StockManagementInterface
{
    /*
     * Inventory Operations functions
     */
    public function cancelReservationQty($reservationOrder, $qtyCancel, $recordInDb = true);

    public function shipReservation($reservationOrder, $qtyShip, $serialsShipped, $shipment = null);

    public function returnReservation($reservationOrder, $qtyReturn, $ReservationSerialsToReturn);

    public function reserveQuoteOrOrder($quoteOrOrderObject, $type);

    public function addOrUpdateReservationsOrdersTableRowFromArray($data, $updateSerializedArrayDbField = true, $useGridData = false);

    public function saveReservation(\SalesIgniter\Rental\Model\ReservationOrdersInterface $reservation, array $data);

    public function deleteReservation(\SalesIgniter\Rental\Model\ReservationOrdersInterface $reservation);

    public function deleteReservationById($idRes);

    public function deleteReservationsByOrderId($orderId);

    public function deleteReservationsByProductId($productId);

    /**
     * Inventory access functions
     */
    public function checkIntervalValid($product, $dates, $quantityToCheck, $baseInventory = null, $storeid=null);

    public function getUpdatedInventoryArray($productId, $startDateWithTurnover, $endDateWithTurnover, $qty, $qtyCancel = 0, $baseInventory = null);

    public function getInventoryArray($product, $source = null, $stockid = null );

    public function updateSirentQuantity($productId, $qty);

    public function getSirentQuantity($product, $stockid=null, $source=null);

    public function getAvailableQuantity($product, $startDate = '', $endDate = '', $excludingReservationsIds=[], $sources=null, $stockid=null, $returnDistribution=false);

    public function getFirstTimeAvailable($product = null);

    public function getFirstDateAvailable($product = null);
}
