<?php
namespace SalesIgniter\Rental\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ReservationOrdersRepositoryInterface
{
    public function getById($idRes);

    public function getByOrderItemId($orderItemId, $source=null);

    public function getList(SearchCriteriaInterface $criteria);

    public function getReservationordersIdForProductandOrderId($sku, $orderid);

    public function getRentalDatesByProductForOrderId($sku, $orderid): array;

    public function getQtyLeftToShipByProductForOrderId($productid, $orderid, $sourceCode): int;

    public function getReservationOrdersByOrderIdCanReturnMultiple($orderid);
}
