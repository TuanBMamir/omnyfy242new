<?php

namespace SalesIgniter\Rental\Model\Order;
use Magento\Sales\Api\Data\OrderItemInterface;
/**
 * Repository class for @see OrderItemInterface
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ItemRepository extends \Magento\Sales\Model\Order\ItemRepository
{

    /**
     * Perform persist operations for one entity
     *
     * @param OrderItemInterface $entity
     * @return OrderItemInterface
     */
    public function save(OrderItemInterface $entity)
    {

        /** comment out product option saving because of https://github.com/magento/magento2/issues/22431 */
//        if ($entity->getProductOption()) {
//            $request = $this->getBuyRequest($entity);
//            $productOptions = $entity->getProductOptions();
//            $productOptions['info_buyRequest'] = $request->toArray();
//            $entity->setProductOptions($productOptions);
//        }

        $this->metadata->getMapper()->save($entity);
        $this->registry[$entity->getEntityId()] = $entity;
        return $this->registry[$entity->getEntityId()];
    }

}
