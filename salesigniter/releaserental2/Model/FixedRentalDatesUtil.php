<?php

namespace SalesIgniter\Rental\Model;

use Carbon\CarbonPeriod;
use League\Period\Period;
use Carbon\Carbon;

class FixedRentalDatesUtil
{
    /**
     * @var \SalesIgniter\Rental\Api\FixedRentalNamesRepositoryInterface
     */
    private $fixedRentalNamesRepository;
    /**
     * @var \SalesIgniter\Rental\Api\FixedRentalDatesRepositoryInterface
     */
    private $fixedRentalDatesRepository;
    /**
     * @var \SalesIgniter\Rental\Helper\Date
     */
    private $helperDate;

    public function __construct(
        \SalesIgniter\Rental\Api\FixedRentalNamesRepositoryInterface $fixedRentalNamesRepository,
        \SalesIgniter\Rental\Api\FixedRentalDatesRepositoryInterface $fixedRentalDatesRepository,
        \SalesIgniter\Rental\Helper\Date $helperDate
    )
    {
        $this->helperDate        = $helperDate;
        $this->fixedRentalNamesRepository = $fixedRentalNamesRepository;
        $this->fixedRentalDatesRepository = $fixedRentalDatesRepository;
    }

    /**
     * uasort method for sorting price asc.
     *
     * @param $firstPeriod
     * @param $secondPeriod
     *
     * @return int
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    public function priceMultiSortAsc( $firstPeriod, $secondPeriod ) {
        return $this->helperDate->compareInterval( $firstPeriod['period'], $secondPeriod['period'] );
    }

    /**
     * uasort method for sorting price asc.
     *
     * @param $firstPeriod
     * @param $secondPeriod
     *
     * @return int
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    public function priceMultiSortDesc( $firstPeriod, $secondPeriod ) {
        return - $this->helperDate->compareInterval( $firstPeriod['period'], $secondPeriod['period'] );
    }

    public function filterPriceListDatesTimes($priceList, $fromDate, $toDate, $currentDate, $currentDateAdditional, $fromToSpecialDateTimeRemovedArray)
    {
        $datesToCheckArray = [];

        if(count($fromToSpecialDateTimeRemovedArray) >= 1){
            $datesToCheckArray = $fromToSpecialDateTimeRemovedArray;
        } else {
            $datesToCheckArray[] = ['startdate'=>$fromDate, 'enddate'=>$toDate];
        }

        foreach($priceList as $priceKey => $priceItem){
            if($priceItem['fixednames_id'] != null){
                $match = false;
                $fixedRentalDatesItem = $this->fixedRentalDatesRepository->getByNameIdAsArray($priceItem['fixednames_id']);
                $specialDatesTimesArray = $this->getSpecialDatesTimesById($priceItem['fixednames_id']);
            foreach($specialDatesTimesArray as $specialDatesTimesItem) {
                $periodSpecial = new Period($specialDatesTimesItem['startdate'],$specialDatesTimesItem['enddate']);
                foreach($datesToCheckArray as $dateToCheckItem)

                    $dateToCheckStart = (is_object($dateToCheckItem['startdate'])) ? $dateToCheckItem['startdate']->format('Y-m-d H:i:s'): $dateToCheckItem['startdate'];
                    $dateToCheckEnd = (is_object($dateToCheckItem['enddate'])) ? $dateToCheckItem['enddate']->format('Y-m-d H:i:s'): $dateToCheckItem['enddate'];
                    $periodToCheck = new Period($dateToCheckStart,$dateToCheckEnd);
                    if($priceItem['fixednames_matchtype'] == 'overlap')
                    {
                        if ($periodSpecial->overlaps($periodToCheck)) {
                            $priceList[$priceKey]['matchType'] = 'overlap';
                            $match = true;
                            $intersect = $periodSpecial->intersect($periodToCheck);
                            $priceList[$priceKey]['matchStart'] = $intersect->getStartDate()->format('Y-m-d H:i:s');
                            $priceList[$priceKey]['matchEnd'] = $intersect->getEndDate()->format('Y-m-d H:i:s');
                        }
                    }
                    if($priceItem['fixednames_matchtype'] == 'contains')
                    {
                        if ($periodSpecial->contains($periodToCheck)) {
                            $priceList[$priceKey]['matchType'] = 'contains';
                            $match = true;
                            $intersect = $periodSpecial->intersect($periodToCheck);
                            $priceList[$priceKey]['matchStart'] = $intersect->getStartDate()->format('Y-m-d H:i:s');
                            $priceList[$priceKey]['matchEnd'] = $intersect->getEndDate()->format('Y-m-d H:i:s');
                        }
                    }
                }
                if($match == false){
                    unset($priceList[$priceKey]);
                }
            }
        }
        usort( $priceList, [ __CLASS__, 'priceMultiSortDesc' ] );
        return $priceList;
    }

    public function filterPriceListOnlySurgePricing($priceList, $fromDate, $toDate, $currentDate, $currentDateAdditional, $fromToSpecialDateTimeRemovedArray)
    {
        $priceListOnlySurgePricing = [];
        $datesToCheckArray = [];
        $priceListFilteredMatchingDatesTimes = [];
        $surgeMatch = [];

        if(count($fromToSpecialDateTimeRemovedArray) >= 1){
            $datesToCheckArray = $fromToSpecialDateTimeRemovedArray;
        } else {
            $datesToCheckArray[] = ['startdate'=>$fromDate, 'enddate'=>$toDate];
        }

        // check if there is surge pricing and if they overlap
        foreach($priceList as $priceKey => $priceItem){
            if($priceItem['fixednames_id'] != null){
                $fixedRentalDatesItem = $this->fixedRentalDatesRepository->getByNameIdAsArray($priceItem['fixednames_id']);
                $specialDatesTimesArray = $this->getSpecialDatesTimesById($priceItem['fixednames_id']);
                foreach($specialDatesTimesArray as $specialDatesTimesItem) {
                    $period = CarbonPeriod::create($specialDatesTimesItem['startdate'],$specialDatesTimesItem['enddate']);
                    foreach($datesToCheckArray as $dateToCheckItem)
                        $dateToCheckStart = (!is_object($dateToCheckItem['startdate'])) ? new \DateTime($dateToCheckItem['startdate']): $dateToCheckItem['startdate'];
                        $dateToCheckEnd = (!is_object($dateToCheckItem['enddate'])) ? new \DateTime($dateToCheckItem['enddate']): $dateToCheckItem['enddate'];
                        if($priceItem['fixednames_surge'] == '1'){
                        if($priceItem['fixednames_matchtype'] == 'overlap')
                        {
                            if ($period->overlaps($dateToCheckStart,$dateToCheckEnd)) {
                                $surgeMatch[] = $priceKey;
                                $priceList[$priceKey]['matchStart'] = $dateToCheckStart->format('Y-m-d H:i:s');
                                $priceList[$priceKey]['matchEnd'] = $dateToCheckEnd->format('Y-m-d H:i:s');
                            }
                        }
                        if($priceItem['fixednames_matchtype'] == 'contains')
                        {
                            if ($period->contains($dateToCheckStart) && $period->contains($dateToCheckEnd)) {
                                $surgeMatch[] = $priceKey;
                                $priceList[$priceKey]['matchStart'] = $dateToCheckStart->format('Y-m-d H:i:s');
                                $priceList[$priceKey]['matchEnd'] = $dateToCheckStart->format('Y-m-d H:i:s');
                            }
                        }
                    }
                }
            }
        }

        // remove all non surge pricing matches so high price is used
        if(count($surgeMatch) > 0){
            $priceListOnlySurgePricing = array_filter($priceList,
            function ($key) use ($surgeMatch) {
                return in_array($key, $surgeMatch);
            }, ARRAY_FILTER_USE_KEY);
        }
        usort( $priceListOnlySurgePricing, [ __CLASS__, 'priceMultiSortDesc' ] );
        return $priceListOnlySurgePricing;
    }

    /**
     * @param $fixedNamesId
     * @return array
     */
    public function getSpecialDatesTimesById($fixedNamesId){
        $fixedDatesArray = $this->fixedRentalDatesRepository->getByNameIdAsArray($fixedNamesId);
        $fixedDatesTimesArray = $this->convertFixedDatesArrayToStartEndDates($fixedDatesArray);
        $test = '';
        return $fixedDatesTimesArray;
    }

    /**
     * @param $fixedDatesArray
     * @return array
     */
    public function convertFixedDatesArrayToStartEndDates($fixedDatesArray){
        $fixedDatesStartEndDates = [];
        foreach($fixedDatesArray as $fixedDatesItem){
            $fixedDatesStartEndDates = array_merge($fixedDatesStartEndDates,$this->convertfixedDateItemToStartEndDates($fixedDatesItem));
        }
        return $fixedDatesStartEndDates;
    }

    /**
     * @param $fixedDatesItem
     * @return array
     * @throws \Exception
     */
    public function convertfixedDateItemToStartEndDates($fixedDatesItem)
    {

        // Daily only use the start and end times
        $fixedStartEndDatesArrayDaily = [];
        if($fixedDatesItem['repeat_type'] == 'daily'){
            // Start the start date max 1 year before today's date and 2 years after today's date
            $startFrom = Carbon::now()->subtract(1,'year'); // - 1 year;
            $endOn2YearsAfter = Carbon::now()->add(2,'year'); // + 2 years;

            $startdate = new Carbon (new \DateTime($fixedDatesItem['date_from']),'UTC');
            $enddate = new Carbon (new \DateTime($fixedDatesItem['date_to']),'UTC');

            $period = CarbonPeriod::create($startFrom, '1 days', $endOn2YearsAfter);
            foreach($period as $date){
                $repeatdaysArray = unserialize($fixedDatesItem['repeat_days']);
                // correlate our days of week to Carbon
                // ours 1 is sunday 2 is monday ... 7 is saturday
                // Carbon 1 is monday 2 is tuesday ... 7 is sunday
                $search = ['1','2','3','4','5','6','7'];
                $replace = ['7','1','2','3','4','5','6'];
                $repeatdaysArray = str_replace($search,$replace,$repeatdaysArray);
                // add if day matches array
                if(in_array($date->dayOfWeekIso,$repeatdaysArray,false)){
                    // add $fixedDatesItem start time and end time with $date y-m-d
                    $startDateCombined = $date->format('Y-m-d') . ' ' . $startdate->format('H:i:s');
                    $endDateCombined = $date->format('Y-m-d') . ' ' . $enddate->format('H:i:s');
                    $fixedStartEndDatesArrayDaily[] = ['startdate'=>$startDateCombined,'enddate'=>$endDateCombined];
                }
            }
        }

        $fixedStartEndDatesArrayWeekly = [];
        if($fixedDatesItem['repeat_type'] == 'weekly'){

        // add 1 week each pass to start and end dates
            $weeksOfMonth = (isset($fixedDatesItem['week_month'])) ? unserialize($fixedDatesItem['week_month']): null;

            $startFrom1YearBefore = Carbon::now()->subtract(1,'year'); // - 1 year;
            $endOn2YearsAfter = Carbon::now()->add(2,'year'); // + 2 years;

            $startdate = new Carbon (new \DateTime($fixedDatesItem['date_from']),'UTC');
            $dayofweek = $startdate->dayOfWeek;
            $timestart = $startdate->format('H:i:s');
            $startdate->setYear($startFrom1YearBefore->year)->next($dayofweek)->next($timestart);
            $enddate = new Carbon (new \DateTime($fixedDatesItem['date_to']),'UTC');
            $endofweek = $enddate->dayOfWeek;
            $endtime = $enddate->format('H:i:s');
            $enddate->setYear($startFrom1YearBefore->year)->next($endofweek)->next($endtime);

            $period = CarbonPeriod::create($startFrom1YearBefore, '1 weeks', $endOn2YearsAfter);


            foreach($period as $date){

                // only add week if it is one of the weeks set by admin (week 1,2,3 etc) or always add if field is not set
                // weeks start on Monday
                if($weeksOfMonth){
                    if(in_array($startdate->weekOfMonth,$weeksOfMonth,false)){
                        $fixedStartEndDatesArrayWeekly[] = ['startdate'=>$startdate->format('Y-m-d H:i:s'),'enddate'=>$enddate->format('Y-m-d H:i:s')];
                    }
                } else {
                    $fixedStartEndDatesArrayWeekly[] = ['startdate'=>$startdate->format('Y-m-d H:i:s'),'enddate'=>$enddate->format('Y-m-d H:i:s')];
                }

                $startdate->addDays(7);
                $enddate->addDays(7);
            }
        }

        // Monthly
        $fixedStartEndDatesArrayMonthly = [];
        if($fixedDatesItem['repeat_type'] == 'monthly'){
        // Add 1 month each pass to start and end dates
            $startFrom = Carbon::now()->subtract(1,'year'); // - 1 year;
            $endOn2YearsAfter = Carbon::now()->add(2,'year'); // + 2 years;
            $startdate = new Carbon (new \DateTime($fixedDatesItem['date_from']),'UTC');
            $startdate->setYear($startFrom->year);
            $enddate = new Carbon (new \DateTime($fixedDatesItem['date_to']),'UTC');
            $enddate->setYear($endOn2YearsAfter->year);
            $period = CarbonPeriod::create($startdate, '1 months', $enddate);
            foreach($period as $date){
                $startdate = new Carbon (new \DateTime($fixedDatesItem['date_from']),'UTC');
                $startdate->setYear($date->year)->setMonth($date->month);
                $enddate = new Carbon (new \DateTime($fixedDatesItem['date_to']),'UTC');
                $enddate->setYear($date->year)->setMonth($date->month);
                $fixedStartEndDatesArrayMonthly[] = ['startdate'=>$startdate->format('Y-m-d H:i:s'),'enddate'=>$enddate->format('Y-m-d H:i:s')];
            }
        }

        // Yearly
        $fixedStartEndDatesArrayYearly = [];
        if($fixedDatesItem['repeat_type'] == 'yearly'){
        // add 1 year each pass to start and end dates
            $startFrom = Carbon::now()->subtract(1,'year'); // - 1 year;
            $endOn = Carbon::now()->add(2,'year'); // + 2 years;
            $startdate = new Carbon (new \DateTime($fixedDatesItem['date_from']),'UTC');
            $startdate->setYear($startFrom->year);
            $enddate = new Carbon (new \DateTime($fixedDatesItem['date_to']),'UTC');
            $enddate->setYear($endOn->year);
            $period = CarbonPeriod::create($startdate, '1 years', $enddate);
            foreach($period as $date){
                $startdate = new Carbon (new \DateTime($fixedDatesItem['date_from']),'UTC');
                $startdate->setYear($date->year);
                $enddate = new Carbon (new \DateTime($fixedDatesItem['date_to']),'UTC');
                $enddate->setYear($date->year);
                $fixedStartEndDatesArrayYearly[] = ['startdate'=>$startdate->format('Y-m-d H:i:s'),'enddate'=>$enddate->format('Y-m-d H:i:s')];
            }
        }

        $fixedStartEndDatesArray = array_merge($fixedStartEndDatesArrayDaily, $fixedStartEndDatesArrayWeekly, $fixedStartEndDatesArrayMonthly, $fixedStartEndDatesArrayYearly);

        return $fixedStartEndDatesArray;
    }

}
