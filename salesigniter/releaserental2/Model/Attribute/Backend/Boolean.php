<?php
/**
 * Copyright © 2017 SalesIgniter. All rights reserved.
 * See https://rentalbookingsoftware.com/license.html for license details.
 */

namespace SalesIgniter\Rental\Model\Attribute\Backend;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Boolean extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \SalesIgniter\Rental\Helper\Calendar
     */
    private $helperCalendar;

    /**
     * @param ScopeConfigInterface                 $scopeConfig
     * @param \SalesIgniter\Rental\Helper\Calendar $helperCalendar
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \SalesIgniter\Rental\Helper\Calendar $helperCalendar
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->helperCalendar = $helperCalendar;
    }

    public function beforeSave($object)
    {
        $attributeCode = $this->getAttribute()->getName();
        if ($object->hasData('use_config_' . $attributeCode) &&
            $object->getData('use_config_' . $attributeCode) === '1'
        ) {
            $object->setData($attributeCode, null);
        } 
        return $this;
    }
}
