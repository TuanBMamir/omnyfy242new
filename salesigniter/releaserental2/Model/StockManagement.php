<?php

namespace SalesIgniter\Rental\Model;

use League\Period\Period;
use Magento\Catalog\Model\Product\Exception;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Model\Order\AddressRepository;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use SalesIgniter\Rental\Api\InventoryGridRepositoryInterface;
use SalesIgniter\Rental\Api\ReservationOrdersRepositoryInterface;
use SalesIgniter\Rental\Model\Attribute\Sources\ExcludedDaysWeekFrom;
use SalesIgniter\Rental\Model\Product\Stock;
use SalesIgniter\Rental\Model\Product\Type\Sirent;
use Magento\Framework\ObjectManagerInterface;


/**
 * Class ReservationOrdersRepository.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.LongVariableNames)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class StockManagement implements \SalesIgniter\Rental\Api\StockManagementInterface {
	/**
	 * @var \SalesIgniter\Rental\Model\ReservationOrdersFactory
	 */
	protected $objectFactory;
	/**
	 * @var \SalesIgniter\Rental\Model\ResourceModel\ReservationOrders\CollectionFactory
	 */
	protected $collectionFactory;

	/**
	 * @var \Magento\Framework\Api\SearchResultsInterfaceFactory
	 */
	protected $searchResultsFactory;
	/**
	 * @var \SalesIgniter\Rental\Model\ResourceModel\ReservationOrders
	 */
	protected $reservationOrderResource;
	/**
	 * @var \SalesIgniter\Rental\Model\Product\Stock
	 */
	protected $stock;
	/**
	 * @var \SalesIgniter\Rental\Helper\Calendar
	 */
	protected $calendarHelper;
	/**
	 * @var \Magento\Framework\Api\SearchCriteriaBuilder
	 */
	protected $searchCriteriaBuilder;
	/**
	 * @var \Magento\Framework\Api\SortOrderBuilder
	 */
	protected $sortOrderBuilder;
	/**
	 * @var \SalesIgniter\Rental\Model\SerialNumberDetailsRepository
	 */
	protected $serialNumberDetailsRepository;
	/**
	 * @var \Magento\Catalog\Model\ProductRepository
	 */
	protected $productRepository;
	/**
	 * @var \Magento\Catalog\Model\Product\Action
	 */
	protected $attributeAction;
	/**
	 * @var \SalesIgniter\Rental\Api\InventoryGridRepositoryInterface
	 */
	protected $inventoryGridRepository;
	/**
	 * @var \Magento\Framework\Stdlib\DateTime\DateTime
	 */
	protected $datetime;
	/**
	 * @var \Magento\Framework\DB\Transaction
	 */
	protected $transaction;
	/**
	 * @var \SalesIgniter\Rental\Api\ReservationOrdersRepositoryInterface
	 */
	private $reservationOrdersRepository;
	/**
	 * @var \SalesIgniter\Rental\Helper\Data
	 */
	private $rentalHelper;
	/**
	 * @var \SalesIgniter\Rental\Helper\Date
	 */
	private $dateHelper;
	/**
	 * @var \SalesIgniter\Rental\Helper\Product
	 */
	private $productHelper;
	/**
	 * @var \Magento\Sales\Api\OrderItemRepositoryInterface
	 */
	private $orderItemRepository;
	/**
	 * @var \Magento\Sales\Api\Data\OrderAddressInterface
	 */
	private $orderAddress;

	/**
	 * @var \Magento\Sales\Model\Order\AddressFactory
	 */
	private $addressFactory;

	/**
	 * @var \Magento\Sales\Model\Order\AddressRepository
	 */
	private $addressRepository;
	/**
	 * @var \Magento\Checkout\Model\Session
	 */
	private $checkoutSession;
	/**
	 * @var \Magento\Backend\Model\Session\Quote
	 */
	private $quoteSession;
    /**
     *
     */
    private $storeRepo;

    private $websiteRepo;

    private $inventoryStockSalesChannelInterface;

    private $getSourcesByPriorityInterface;

    /**
     * @var
     */
    private $bysourceFactory;

    /**
     * @var
     */
    private $bysourceRepository;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
	 * ReservationOrdersRepository constructor.
	 *
	 * @param \SalesIgniter\Rental\Model\ReservationOrdersFactory                                                          $objectFactory
	 * @param \SalesIgniter\Rental\Helper\Calendar                                                                         $calendarHelper
	 * @param \SalesIgniter\Rental\Helper\Data                                                                             $rentalHelper
	 * @param \SalesIgniter\Rental\Helper\Product                                                                          $productHelper
	 * @param \SalesIgniter\Rental\Helper\Date                                                                             $dateHelper
	 * @param \SalesIgniter\Rental\Model\ResourceModel\ReservationOrders\CollectionFactory                                 $collectionFactory
	 * @param \SalesIgniter\Rental\Model\ResourceModel\ReservationOrders                                                   $reservationOrderResource
	 * @param \Magento\Framework\Stdlib\DateTime\DateTime                                                                  $datetime
	 * @param \SalesIgniter\Rental\Model\SerialNumberDetailsRepository                                                     $serialNumberDetailsRepository
	 * @param Stock                                                                                                        $stock
	 * @param \SalesIgniter\Rental\Api\ReservationOrdersRepositoryInterface                                                $reservationOrdersRepository
	 * @param \Magento\Framework\Api\SortOrderBuilder                                                                      $sortOrderBuilder
	 * @param \SalesIgniter\Rental\Api\InventoryGridRepositoryInterface                                                    $inventoryGridRepository
	 * @param \Magento\Catalog\Model\ProductRepository                                                                     $productRepository
	 * @param \Magento\Catalog\Model\Product\Action                                                                        $attributeAction
	 * @param \Magento\Framework\Api\SearchCriteriaBuilder                                                                 $searchCriteriaBuilder
	 * @param \Magento\Framework\DB\Transaction                                                                            $transaction
	 * @param \Magento\Framework\Api\SearchResultsInterfaceFactory                                                         $searchResultsFactory
	 * @param \Magento\Sales\Api\OrderItemRepositoryInterface                                                              $orderItemRepository
	 * @param \Magento\Sales\Api\Data\OrderAddressInterface                                                                $orderAddress
	 * @param \Magento\Sales\Model\Order\AddressRepository                                                                 $addressRepository
	 * @param \Magento\Checkout\Model\Session                                                                              $checkoutSession
	 * @param \Magento\Backend\Model\Session\Quote                                                                         $quoteSession
	 * @param \Magento\Sales\Model\Order\AddressFactory|\Magento\Sales\Model\ResourceModel\Order\Address\CollectionFactory $addressFactory
	 */
	public function __construct(
		\SalesIgniter\Rental\Model\ReservationOrdersFactory $objectFactory,
		\SalesIgniter\Rental\Helper\Calendar $calendarHelper,
		\SalesIgniter\Rental\Helper\Data $rentalHelper,
		\SalesIgniter\Rental\Helper\Product $productHelper,
		\SalesIgniter\Rental\Helper\Date $dateHelper,
		\SalesIgniter\Rental\Model\ResourceModel\ReservationOrders\CollectionFactory $collectionFactory,
		\SalesIgniter\Rental\Model\ResourceModel\ReservationOrders $reservationOrderResource,
		\Magento\Framework\Stdlib\DateTime\DateTime $datetime,
		SerialNumberDetailsRepository $serialNumberDetailsRepository,
		Stock $stock,
		ReservationOrdersRepositoryInterface $reservationOrdersRepository,
		SortOrderBuilder $sortOrderBuilder,
		InventoryGridRepositoryInterface $inventoryGridRepository,
		ProductRepository $productRepository,
		\Magento\Catalog\Model\Product\Action $attributeAction,
		SearchCriteriaBuilder $searchCriteriaBuilder,
		\Magento\Framework\DB\Transaction $transaction,
		SearchResultsInterfaceFactory $searchResultsFactory,
		OrderItemRepositoryInterface $orderItemRepository,
		OrderAddressInterface $orderAddress,
		AddressRepository $addressRepository,
		\Magento\Checkout\Model\Session $checkoutSession,
		\Magento\Backend\Model\Session\Quote $quoteSession,
		\Magento\Sales\Model\Order\AddressFactory $addressFactory,
        StoreRepositoryInterface $storeRepo,
        WebsiteRepositoryInterface $websiteRepo,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\RequestInterface $request

	) {
	    $this->request = $request;
		$this->searchResultsFactory          = $searchResultsFactory;
		$this->objectFactory                 = $objectFactory;
		$this->collectionFactory             = $collectionFactory;
		$this->reservationOrderResource      = $reservationOrderResource;
		$this->stock                         = $stock;
		$this->calendarHelper                = $calendarHelper;
		$this->searchCriteriaBuilder         = $searchCriteriaBuilder;
		$this->sortOrderBuilder              = $sortOrderBuilder;
		$this->serialNumberDetailsRepository = $serialNumberDetailsRepository;
		$this->productRepository             = $productRepository;
		$this->attributeAction               = $attributeAction;
		$this->inventoryGridRepository       = $inventoryGridRepository;
		$this->datetime                      = $datetime;
		$this->transaction                   = $transaction;
		$this->reservationOrdersRepository   = $reservationOrdersRepository;
		$this->rentalHelper                  = $rentalHelper;
		$this->dateHelper                    = $dateHelper;
		$this->productHelper                 = $productHelper;
		$this->orderItemRepository           = $orderItemRepository;
		$this->orderAddress                  = $orderAddress;
		$this->addressFactory                = $addressFactory;
		$this->addressRepository             = $addressRepository;
		$this->checkoutSession               = $checkoutSession;
		$this->quoteSession                  = $quoteSession;
		$this->storeRepo                     = $storeRepo;
		$this->websiteRepo                   = $websiteRepo;
        $this->objectManager = $objectManager;
	}

    /**
     * Useful for debugging inventory issues instead of using serialized array
     * it builds inventory array from scratch from reservation orders table
     *
     * @param $productId
     * @return array
     */

    public function getInventoryArrayFromScratch($productId)
    {
        $this->searchCriteriaBuilder->addFilter( 'main_table.product_id', $productId );
        $this->searchCriteriaBuilder->addFilter( 'qty_use_grid', 0, 'gt' );

        // TODO maybe add filter for end date after today's date to reduce overhead
        $sortOrder = $this->sortOrderBuilder->setField('start_date_with_turnover')->setDirection('ASC')->create();
        $criteria = $this->searchCriteriaBuilder->setSortOrders([$sortOrder])->create();

//        $criteria = $this->searchCriteriaBuilder->create();
        $reservationsBySourceCode = $this->reservationOrdersRepository->getList( $criteria )->getItems();

        // loop through each reservation orders and update Inventory Array
        $inventoryArray=[];
        foreach($reservationsBySourceCode as $reservation){
            $startDateWithTurnover = $reservation->getStartDateUseGrid();
            $endDateWithTurnover = $reservation->getEndDateUseGrid();
            $qty = $reservation->getQtyUseGrid();
            $ordertype = $reservation->getOrderType();
            $inventoryArray = $this->createOrUpdateInventoryArray($startDateWithTurnover, $endDateWithTurnover, $qty, $inventoryArray, $this->convertLongToShortOrderType($ordertype));
            $inventoryArray = $this->stock->normalizeInventory( $inventoryArray );
            $inventoryArray = $this->stock->compactInventory( $inventoryArray );
        }


        return $inventoryArray;
    }

    public function updateFromTableSerializedArray($productid) {
        $array = $this->getInventoryArrayFromScratch($productid);
        $array = $this->stock->compactInventory($array);
        $array = $this->stock->normalizeInventory($array);
        $this->saveSerializedInvProductAttribute( $productid, $array );
        return $array;
    }


	/**
	 * @param $data
	 *
	 * @return mixed
	 */
	private function convertDatePartOfDataToDatetime($data ) {
		if ( ! is_object( $data['start_date_with_turnover'] ) ) {
			$data['start_date_with_turnover'] = new \DateTime( $data['start_date_with_turnover'] );
		}
		if ( ! is_object( $data['end_date_with_turnover'] ) ) {
			$data['end_date_with_turnover'] = new \DateTime( $data['end_date_with_turnover'] );
		}
		if ( ! is_object( $data['start_date'] ) ) {
			$data['start_date'] = new \DateTime( $data['start_date'] );
		}
		if ( ! is_object( $data['end_date'] ) ) {
			$data['end_date'] = new \DateTime( $data['end_date'] );

			return $data;
		}

		return $data;
	}

    /**
     * Used by Manual Reserve and Maintenance Tickets to update existing reservations
     *
     * qty_use_grid: Qty use grid is used to know how much quantity in the current table row should be used in inventory
     * calculations. It may need to be reduced since when shipping a duplicate table row is created with parent_id
     *
     * start_date_use_grid: usually is same as start date with turnover, but can be affected by ship date. Rental may have an early ship date.
     * The original reservationorders row will have the start_date_use_grid the same as the start_date_with_turnover but the new row with parent_id
     * may have a different start_date_use_grid if sent early.
     *
     * end_date_use_grid: usually is same as end date with turnover, but can be affected by return date. Rental may have an early return. The
     * original reservationorders row will have the end_date_use_grid the same as the end_date_with_turnover but the new row with parent_id
     * may have a different end_date_use_grid if returned early.
     *
     * parent_id: Used to hold the parent reservationorder_id as we add a new row when reservation orders are shipped, then we change the original rows qty_shipped and
     * new row qty_shipped, and change the old row qty_use_grid to be reduced by the shipped qty.
     *
     * order_type: like 'order', 'rfq', 'maintenance', 'manual'
     *
     * @param ReservationOrdersInterface $reservation
     * @param array $data
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */

	public function saveReservation( \SalesIgniter\Rental\Model\ReservationOrdersInterface $reservation, array $data ) {
		$dataArray = [];
		if ( empty( $data['start_date'] ) || $data['start_date'] == 'Invalid date' ) {
			$data['start_date'] = '0000-00-00 00:00:00';
		}
		if ( empty( $data['end_date'] ) || $data['end_date'] == 'Invalid date' ) {
			$data['end_date'] = '0000-00-00 00:00:00';
		}
		if ( empty( $data['reservationorder_id'] ) ) {
			$data['reservationorder_id'] = null;
		}

		/**
         * Not sure why, but we don't allow them to set the start / end dates with turnover
         * assumed this is auto-calculated later
         */
		unset( $data['start_date_with_turnover'] );
		unset( $data['end_date_with_turnover'] );
		unset( $data['start_date_use_grid'] );
		unset( $data['end_date_use_grid'] );
		unset( $data['qty_use_grid'] );

		$data['qty_use_grid'] = ((int)$data['qty']) - ((int)$data['qty_shipped']) - ((int)$data['qty_cancel']);

		$dataArray[] = $data;


		if ( $reservation->getReservationorderId() && $reservation->getReservationorderId() !== null ) {
            $this->cancelReservationQty( $reservation, $reservation->getQtyUseGrid(), false );

            /**
             * If there is already a reservation where qty_use_grid is >= 1 for this order and parent_id (order shipped)
             * Cancel them too
             */
			$this->searchCriteriaBuilder->addFilter( 'parent_id', $reservation->getReservationorderId() );
			$this->searchCriteriaBuilder->addFilter( 'qty_use_grid', 0, 'gt' );
			$criteria = $this->searchCriteriaBuilder->create();
			$items    = $this->reservationOrdersRepository->getList( $criteria )->getItems();

			foreach ( $items as $item ) {
				$this->cancelReservationQty( $item, $item->getQtyUseGrid(), false );
				$dataItem             = $item->toArray( [] );
				$dataItem['end_date'] = $data['end_date'];
				unset( $dataItem['end_date_with_turnover'] );
				unset( $dataItem['end_date_use_grid'] );
				$dataArray[] = $dataItem;
			}
		}
		foreach ( $dataArray as $iData ) {
			$this->addOrUpdateReservationsOrdersTableRowFromArray( $iData, true, true );
		}
	}

	public function deleteReservation( \SalesIgniter\Rental\Model\ReservationOrdersInterface $reservation ) {
		try {
			$this->searchCriteriaBuilder->addFilter( 'parent_id', $reservation->getReservationorderId() );
			$criteria = $this->searchCriteriaBuilder->create();
			$items    = $this->reservationOrdersRepository->getList( $criteria )->getItems();
			$this->cancelReservationQty( $reservation, $reservation->getQtyUseGrid(), false );
			$reservation->delete();
			foreach ( $items as $item ) {
				$this->cancelReservationQty( $item, $item->getQtyUseGrid(), false );
				$item->delete();
			}
		} catch ( Exception $exception ) {
			throw new CouldNotDeleteException( __( $exception->getMessage() ) );
		}

		return true;
	}

	public function deleteReservationById( $idRes ) {
		return $this->deleteReservation( $this->reservationOrdersRepository->getById( $idRes ) );
	}

	public function deleteReservationsByOrderId( $orderId ) {
		$this->searchCriteriaBuilder->addFilter( 'main_table.order_id', $orderId );
		$criteria = $this->searchCriteriaBuilder->create();
		$items    = $this->reservationOrdersRepository->getList( $criteria )->getItems();

		foreach ( $items as $item ) {
			$this->deleteReservation( $item );
		}

		return count( $items ) > 0;
	}

	public function deleteReservationsByProductId( $productId ) {
		$this->searchCriteriaBuilder->addFilter( 'main_table.product_id', $productId );
		$criteria = $this->searchCriteriaBuilder->create();
		$items    = $this->reservationOrdersRepository->getList( $criteria )->getItems();

		foreach ( $items as $item ) {
			$this->deleteReservation( $item );
		}
		if ( count( $items ) > 0 ) {
			return true;
		}

		return false;
	}

	/**
	 * Saves a reservation to the reservationorders table. You could call this the "Main Entry Point" for saving reservations.
     * Optionally update stock (Inventory Array)
     *
     * If you are updating an existing reservation, you should use the saveReservation method.
	 *
	 * @param array $data
	 *                           $data['start_date'] - YYYY-MM-DD HH:MM:SS
	 *                           $data['end_date'] - YYYY-MM-DD HH:MM:SS
	 *                           $data['start_date_with_turnover'] - YYYY-MM-DD HH:MM:SS optional if not used is auto-calculated
	 *                           $data['end_date_with_turnover'] - YYYY-MM-DD HH:MM:SS optional if not used is auto-calculated
	 *                           $data['qty'] - quantity to reserve
     *                           $data['not_use_turnover'] - don't use turnover
	 *                           $data['product_id'] - product id to reserve
	 *                           $data['serials_shipped'] - optional comma seperated string of serials that are shipped
	 *                           $data['serials_returned'] - optional comma seperated string of serials returned
	 *                           $data['reservationorder_id'] - if updating reservation set the id here
     *                           $data['order_type'] - long form like: rfq, maintenance, order
	 * @param bool  $updateSerializedArrayDbField
	 * @param bool  $useGridData
	 *
	 * @return \SalesIgniter\Rental\Model\ReservationOrdersInterface
	 *
	 * @throws \Magento\Framework\Exception\LocalizedException
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 * @throws \LogicException
	 */
	public function addOrUpdateReservationsOrdersTableRowFromArray($data, $updateSerializedArrayDbField = true, $useGridData = false ) {
		$reservationOrder = $this->objectFactory->create();
		if ( ! array_key_exists( 'end_date', $data ) || empty( $data['end_date'] ) ) {
			$data['end_date'] = '0000-00-00 00:00:00';
		}
		if ( array_key_exists( 'start_date_use_grid', $data ) ) {
			$data['start_date_with_turnover'] = $data['start_date_use_grid'];
		}
		if ( array_key_exists( 'end_date_use_grid', $data ) ) {
			$data['end_date_with_turnover'] = $data['end_date_use_grid'];
		}
		if ( ! array_key_exists( 'start_date_with_turnover', $data ) ) {
			if ( ! array_key_exists( 'not_use_turnover', $data ) ) {
			    if(isset($data['storeid'])){
			        $storeid = $data['storeid'];
                } else {
                    $storeid = null;
                }
				$data['start_date_with_turnover'] = $this->calendarHelper->getActualTurnoverBeforeDate( $data['product_id'], $data['start_date'], $storeid);
			} else {
				$data['start_date_with_turnover'] = $data['start_date'];
			}
		}
		if ( ! array_key_exists( 'end_date_with_turnover', $data ) ) {
			if ( $data['end_date'] === '0000-00-00 00:00:00' ) {
				$data['end_date_with_turnover'] = '0000-00-00 00:00:00';
			} else {
				if ( ! array_key_exists( 'not_use_turnover', $data ) ) {
                    if(isset($data['storeid'])){
                        $storeid = $data['storeid'];
                    } else {
                        $storeid = null;
                    }
					$data['end_date_with_turnover'] = $this->calendarHelper->getActualTurnoverAfterDate( $data['product_id'], $data['end_date'], $storeid );
				} else {
					$data['end_date_with_turnover'] = $data['end_date'];
				}
			}
		}
		if ( ! array_key_exists( 'start_date_use_grid', $data ) ) {
			$data['start_date_use_grid'] = $data['start_date_with_turnover'];
			if ( is_object( $data['start_date_with_turnover'] ) ) {
				$data['start_date_use_grid'] = $data['start_date_with_turnover']->format( 'Y-m-d H:i:s' );
			}
		}
		if ( ! array_key_exists( 'end_date_use_grid', $data ) ) {
			$data['end_date_use_grid'] = $data['end_date_with_turnover'];
			if ( is_object( $data['end_date_with_turnover'] ) ) {
				$data['end_date_use_grid'] = $data['end_date_with_turnover']->format( 'Y-m-d H:i:s' );
			}
		}
		/*
		 * because for some reason my install don't have default values I do the check here
		 * normally upgrade schema should make default values to 0
		 */
		if ( ! array_key_exists( 'qty_shipped', $data ) ) {
			$data['qty_shipped'] = 0;
		}
		if ( ! array_key_exists( 'qty_returned', $data ) ) {
			$data['qty_returned'] = 0;
		}
		if ( ! array_key_exists( 'qty_cancel', $data ) ) {
			$data['qty_cancel'] = 0;
		}
		/*
		 * end of checked
		 */

		if ( ! array_key_exists( 'qty_use_grid', $data ) ) {
			$data['qty_use_grid'] = $data['qty'];
		}
		$reservationOrder->setData( $data );
		try {
			$returnData = $reservationOrder->save();
		} catch ( Exception $e ) {
			throw new CouldNotSaveException( $e->getMessage() );
		}

		if ( $updateSerializedArrayDbField ) {
			if ( $useGridData ) {
				if ( array_key_exists( 'end_date_use_grid', $data ) ) {
					$data['end_date_with_turnover'] = $data['end_date_use_grid'];
				}
				if ( array_key_exists( 'start_date_use_grid', $data ) ) {
					$data['start_date_with_turnover'] = $data['start_date_use_grid'];
				}
				if ( array_key_exists( 'qty_use_grid', $data ) ) {
					$data['qty']        = $data['qty_use_grid'];
					$data['qty_cancel'] = 0;
				}
			}
			$this->updateInventory( $data );
		}

		return $returnData;
	}

    /**
     * Deprecated - to remove
     *
     * @param $data
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */

	public function updateStockFromGridData( $data ) {
		$data['not_check_valid'] = true;
		if ( array_key_exists( 'end_date_use_grid', $data ) ) {
			$data['end_date_with_turnover'] = $data['end_date_use_grid'];
		}
		if ( array_key_exists( 'start_date_use_grid', $data ) ) {
			$data['start_date_with_turnover'] = $data['start_date_use_grid'];
		}
		if ( array_key_exists( 'qty_use_grid', $data ) ) {
			$data['qty']        = $data['qty_use_grid'];
			$data['qty_cancel'] = 0;
		}

		$this->updateInventory( $data );
	}

	/**
	 * Updates Inventory Array for a product using $data array and save to DB
     *
	 * $data must have product_id, start_date_with_turnover, end_date_with_turnover
	 * and optional order_id and qty. This function is used to actually update the inventory in the product serialized field.
	 *
	 * @param \Magento\Framework\DataObject | array $data
	 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
	 *
	 * @throws \LogicException
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 * @throws \Magento\Framework\Exception\LocalizedException
     *
     * $data['order_type'] should have long form order type like 'rfq', 'order', etc.
	 */
	public function updateInventory( $data ) {
		if ( is_object( $data ) ) {
			$data = $data->getData();
		}
		/** @var array $data */
		if ( array_key_exists( 'product_id', $data ) &&
		     array_key_exists( 'start_date_with_turnover', $data ) &&
		     array_key_exists( 'end_date_with_turnover', $data )
		) {
			$productId             = $data['product_id'];
			$startDateWithTurnover = $data['start_date_with_turnover'];
			$endDateWithTurnover   = $data['end_date_with_turnover'];

			$data          = $this->convertDatePartOfDataToDatetime( $data );
			$qty           = array_key_exists( 'qty', $data ) ? $data['qty'] : 0;
			$qtyCancel     = array_key_exists( 'qty_cancel', $data ) ? $data['qty_cancel'] : 0;
			$notCheckValid = array_key_exists( 'not_check_valid', $data ) ? true : false;
			$orderId       = array_key_exists( 'order_id', $data ) ? $data['order_id'] : 0;
			$dates         = new \Magento\Framework\DataObject( $data );

			$storeid = null;
			if(isset($data['store_id'])){
			    $storeid = $data['store_id'];
            }
			if ( $notCheckValid || $data['end_date'] === '0000-00-00 00:00:00' || $data['start_date'] === '0000-00-00 00:00:00' || $this->checkIntervalValid( $productId, $dates, $qty, null, $storeid ) === Stock::NO_ERROR ) {
				$type = $this->convertLongToShortOrderType($data['order_type']);
			    $updatedInventoryArray = $this->getUpdatedInventoryArray($productId, $startDateWithTurnover, $endDateWithTurnover, $qty, $qtyCancel, null, $type);
				$this->saveSerializedInvProductAttribute( $productId, $updatedInventoryArray );
				if($this->rentalHelper->isRentalInventoryEnabled()){
                    $stockManager = $this->objectManager->create('SalesIgniter\Rentalinventory\Model\StockManagement');
                    if ( $qtyCancel > 0 ) {
                        $qty = - $qtyCancel;
                    }
                    $sourceCode = $data['source_code'];
                    $stockManager->updateBySourceAndByStock($productId, $sourceCode, $startDateWithTurnover, $endDateWithTurnover, $qty, $type);
                    }
                }
			}
		}

    /**
     * Inventory Array needs short order types like 'o' for order or 'r' for rfq
     *
     * @param $longOrderType
     * @return string
     */
		public function convertLongToShortOrderType($longOrderType)
        {
            $shortOrderType = 'o';
            switch ($longOrderType) {
                case 'order':
                    $shortOrderType = 'o';
                    break;
                case 'maintenance':
                    $shortOrderType = 'm';
                    break;
                case 'manual':
                    $shortOrderType = 'l';
                    break;
                case 'rfq':
                    $shortOrderType = 'r';
                    break;
            }
            return $shortOrderType;
        }

	/**
	 * Takes a product id and $updatedInventory array and updates the serialized field.
	 *
	 * @param in $productId
	 * @param    $updatedInventory
	 *
	 * @return array
	 *
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	private function saveSerializedInvProductAttribute($productId, $updatedInventory ) {

	    // deprecated, commenting out to remove later
	    // $this->updateGridTableWithData( $updatedInventory, $productId );

		$this->attributeAction->updateAttributes(
			[ $productId ],
			[ 'sirent_inv_bydate_serialized' => serialize( $updatedInventory ) ],
			0
		);
	}

	/**
	 * Takes a product id and $updatedInventory (serialized array with both
	 * old and new reservations combined) and updates the serialized field.
	 *
	 * @param int $productId
	 * @param int $qty
	 *
	 * @return bool
	 *
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	public function updateSirentQuantity( $productId, $qty ) {

		$this->attributeAction->updateAttributes(
			[ $productId ],
			[ 'sirent_quantity' => $qty ],
			0
		);

		return true;
	}

    /**
     * Cancels inventory for a Reservationorder item
     *
     * @param $reservationOrder
     * @param $qtyCancel
     * @param bool $recordInDb
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */

	public function cancelReservationQty( $reservationOrder, $qtyCancel, $recordInDb = true ) {
		$qtyCancel = (int) $qtyCancel;
		$data      = $reservationOrder->getData();
		if ( ! $recordInDb ) {
			$data['start_date_with_turnover'] = $data['start_date_use_grid'];
			$data['end_date_with_turnover']   = $data['end_date_use_grid'];
		}
		$data['qty']        = 0;
		$data['qty_cancel'] = $qtyCancel;

		if ( $recordInDb ) {
			$reservationOrder->setQtyCancel( $reservationOrder->getQtyCancel() + $qtyCancel );
			$reservationOrder->setQtyUseGrid( $reservationOrder->getQtyUseGrid() - $qtyCancel );
			try {
				$reservationOrder->save();
			} catch ( Exception $e ) {
				throw new CouldNotSaveException( $e->getMessage() );
			}

			$data['parent_id']    = $reservationOrder->getId();
			$data['qty_use_grid'] = 0;
			unset( $data['reservationorder_id'] );
			$this->addOrUpdateReservationsOrdersTableRowFromArray( $data );
		} else {
			$this->updateInventory( $data );
		}
	}

    /**
     * Called from ShipmentSaveCommitted Observer
     *
     * Updates sirental_reservationsorders with shipment qty and serials
     *
     * Updates sirental_serialnumber_details setting serials to out that were shipped
     *
     * Updates inventory tables if there was an early send
     *
     * @param $reservationOrder
     * @param $qtyShip
     * @param $serialsShipped
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
	public function shipReservation($reservationOrder, $qtyShip, $serialsShipped, $shipment = null) {
		$qtyShip             = (int) $qtyShip;
		$data                = $reservationOrder->getData();
		$shipDate            = $this->calendarHelper->approximateDateFromSetting( 'now', $data['product_id'] );
		$shipDateFormatted   = $shipDate->format( 'Y-m-d H:i:s' );
		$data['qty_shipped'] = $qtyShip;
		$data['ship_date']   = $shipDateFormatted;
		$isEarly             = false;



		if ( new \DateTime( $reservationOrder->getStartDateWithTurnover() ) > new \DateTime( $data['ship_date'] ) ) {
			$isEarly = true;
		}

        /**
         * If is early ship, reservation orders row needs to have qty use grid set to how many were shipped
         * original parent record needs to be reduced too
         */
		if ( $isEarly && $this->stock->reserveInventoryEarlySendDate() ) {
			$reservationOrder->setQtyUseGrid( $reservationOrder->getQtyUseGrid() - $qtyShip );

			$this->cancelReservationQty( $reservationOrder, $qtyShip, false );

			$data['start_date_use_grid'] = $shipDateFormatted;
			$data['end_date_use_grid']   = $data['end_date_with_turnover'];
			$data['qty_use_grid']        = $qtyShip;
		}

		$this->updateSerialnumberDetailsTableToOut( $reservationOrder, $qtyShip, $serialsShipped, $data );

		// Update original reservation order id row
		try {
			$reservationOrder->save();
		} catch ( Exception $e ) {
			throw new CouldNotSaveException( $e->getMessage() );
		}

        /**
         * New row for shipment should be 0 for qty use grid unless is early shipment
         */
        $data['qty_use_grid'] = 0;

		$data['parent_id']  = $reservationOrder->getId();
		$data['qty_cancel'] = 0;


        // unset reservation id so that a new row will be created for the shipment
		unset( $data['reservationorder_id'] );

        // add shipment id to reservation orders table
        if($qtyShip >= 0 && is_object($shipment)) {
            $data['sales_shipment_id'] = $shipment->getId();
            //$reservationOrder->setSalesShipmentId($shipment->getId());
        }

		$reservationorderShipmentRow = $this->addOrUpdateReservationsOrdersTableRowFromArray( $data, false );

		//there is no way to force commit transactions
		//basically here no data is saved into tables until after commit and so the inventory grid can't be updated properly
		//this happens only when transaction object is used, since we do the actions on model save before.
		//doing it into the controller plugin might be the only solution

		if ( $isEarly && $this->stock->reserveInventoryEarlySendDate() ) {
			$dataForInventory                             = $data;
			$dataForInventory['start_date_with_turnover'] = $shipDateFormatted;
			$dataForInventory['qty']                      = $qtyShip;
			$dataForInventory['not_check_valid']          = true;
			$this->updateInventory( $dataForInventory );
		}

		return $reservationorderShipmentRow;
	}

    /**
     * Updates sirental_reservationorders table with return information
     *
     * Also updates serials in the sirental_serialnumber_details table to available
     *
     * Updates inventory table if early returns
     *
     * @param $reservationOrder object
     * @param $qtyReturn int
     * @param $ReservationSerialsToReturn array
     * @return bool|int|void
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
	public function returnReservation($reservationOrder, $qtyReturn, $ReservationSerialsToReturn ) {
		$qtyReturn  = (int) $qtyReturn;
		$data       = $reservationOrder->getData();
		$returnDate = $this->calendarHelper->approximateDateFromSetting( 'now', $data['product_id'] );
		$returnDate->add( new \DateInterval( 'PT1H' ) );
		$data['return_date']  = $returnDate->format( 'Y-m-d H:i:s' );
		$data['qty_returned'] = $qtyReturn;

		$criteria = $this->getShippedItems( $reservationOrder );

		$resorderShippedItems              = $this->reservationOrdersRepository->getList( $criteria )->getItems();
		$totalItemsReturned = 0;
		$qtyReturnOriginal  = $qtyReturn;

		foreach ( $resorderShippedItems as $resorderShippedItem ) {
			$serialsShippedItem  = array_filter( explode( ',', $resorderShippedItem->getSerialsShipped() ), function ( $value ) {
				return $value !== '';
			} );
			$serialsShippedItem = array_map('trim', $serialsShippedItem);
			$currentSerialsToReturn = [];

            // Remove any empty serials from array
			if ( is_array( $ReservationSerialsToReturn ) ) {
				$ReservationSerialsToReturn = array_filter( $ReservationSerialsToReturn, function ($value ) {
					return $value !== '';
				} );
			} else {
				$ReservationSerialsToReturn = [];
			}

			// Make sure serials selected for return match the ones that were shipped
			if ( count( $serialsShippedItem ) > 0 ) {
				$currentSerialsToReturn = array_intersect( $ReservationSerialsToReturn, $serialsShippedItem );
				$currentQtyReturn    = count( $currentSerialsToReturn );

				// add ability to return even if serials don't match shipment
                if($this->stock->allowReturningNonMatchedSerials()){
                    $currentQtyReturn = $resorderShippedItem->getQtyShipped();
                }

				if ( $currentQtyReturn === 0 ) {
					continue;
				}
			} else {
				$currentQtyReturn = $resorderShippedItem->getQtyShipped();
			}
			if ( $currentQtyReturn > $qtyReturn ) {
				$currentQtyReturn = $qtyReturn;
			}

			$isEarly = false;
			if ( new \DateTime( $reservationOrder->getEndDateWithTurnover() ) > new \DateTime( $data['return_date'] ) ) {
				$isEarly = true;
			}
			$isNew = false;
			if ( $resorderShippedItem->getReturnDate() ) {
				$isNew = true;
			}
			$shipDate            = $resorderShippedItem->getShipDate();
			$returnDateFormatted = $data['return_date'];

			$resorderShippedItem->setQtyReturned( $currentQtyReturn );
			$resorderShippedItem->setReturnDate( $returnDateFormatted );
			$resorderShippedItem->setQtyUseGrid( 0 );

			// Releases original inventory and sets early return data
			if ( $isEarly && $this->stock->reserveInventoryEarlyReturnDate() ) {
				$this->cancelReservationQty( $resorderShippedItem, $currentQtyReturn, false );
				$resorderShippedItem->setQtyUseGrid( $currentQtyReturn );
				$resorderShippedItem->setStartDateUseGrid( $shipDate );
				$resorderShippedItem->setEndDateUseGrid( $returnDateFormatted );
			}

			$this->updateSerialnumberDetailsTableToAvailable( $reservationOrder, $currentSerialsToReturn, $currentQtyReturn, $resorderShippedItem );

			if ( ! $isNew ) {
				try {
					$resorderShippedItem->save();
				} catch ( Exception $e ) {
					throw new CouldNotSaveException( $e->getMessage() );
				}
			} else {
				$newData = $resorderShippedItem->getData();
				unset( $newData['reservationorder_id'] );
				$this->addOrUpdateReservationsOrdersTableRowFromArray( $newData, false );
			}
			try {
				$reservationOrder->save();
			} catch ( Exception $e ) {
				throw new CouldNotSaveException( $e->getMessage() );
			}

			// Record early return data to inventory array
			if ( $isEarly && $this->stock->reserveInventoryEarlyReturnDate() ) {
				$dataForInventory                             = $data;
				$dataForInventory['start_date_with_turnover'] = $shipDate;
				$dataForInventory['end_date_with_turnover']   = $returnDateFormatted;
				$dataForInventory['qty']                      = $currentQtyReturn;
				$dataForInventory['qty_cancel']               = 0;
				$dataForInventory['not_check_valid']          = true;
				$this->updateInventory( $dataForInventory );
			}
			$totalItemsReturned = $totalItemsReturned + $currentQtyReturn;
			/****************/

			$qtyReturn    -= $currentQtyReturn;
			$ReservationSerialsToReturn = array_diff( $ReservationSerialsToReturn, $serialsShippedItem );
			if ( $qtyReturn <= 0 ) {
				break;
			}
		}

		// Not enough items were returned
		if ( $totalItemsReturned !== $qtyReturnOriginal ) {
			return false;
		}

		return $totalItemsReturned;
	}


    /**
     * Reserves a New Order by iterating through each order item
     *
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $quoteOrOrderObject
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */

	public function reserveQuoteOrOrder( $quoteOrOrderObject, $type='order')
    {

        /** @var \Magento\Sales\Api\Data\OrderItemInterface $item */
        // if order was already reserved, skip
        foreach ($quoteOrOrderObject->getAllItems() as $item) {
            $reservationOrderExisting = $this->reservationOrdersRepository->getByOrderItemId($item->getId());
            if ($reservationOrderExisting !== null) {
                break;
            }
            if ($item->getProductType() === Sirent::TYPE_RENTAL) {
                if ($this->calendarHelper->getDisabledShipping($item->getProductId())) {
                    $this->reserveDisableShipping($item, $quoteOrOrderObject);
                }
                if ($item->getParentItem()) {
                    $buyRequest = $this->calendarHelper->prepareBuyRequest($item->getParentItem());
                } else {
                    $buyRequest = $this->calendarHelper->prepareBuyRequest($item);
                }

                $dates = $this->calendarHelper->getDatesFromBuyRequest(
                    $buyRequest, $item->getProductId()
                );

                $sourceCode = 'default';
                $qty = ($item->getQtyOrdered() != null) ? $item->getQtyOrdered() : $item->getQty();

                if (!$dates->getIsBuyout()) {
                    $startDateWithTurnover = $dates->getStartDateWithTurnover()->format('Y-m-d H:i:s');
                    $endDateWithTurnover = $dates->getEndDateWithTurnover()->format('Y-m-d H:i:s');
                }

                if ($this->rentalHelper->isRentalInventoryEnabled() && !$dates->getIsBuyout()) {

                    /**
                     * Used for MSI to get quantity to reserve by sources
                     */

                    $storeid = $item->getStoreId();
                    $stockManager = $this->objectManager->create('SalesIgniter\Rentalinventory\Model\StockManagement');
                    $stockid = $stockManager->getStockIdFromStoreId($storeid);
                    $availabilityArray = $stockManager->getAvailabilityByStockForProduct($startDateWithTurnover, $endDateWithTurnover, $stockid, $item->getSku());
                    $howMuchToReserveBySourceArray = $stockManager->distributeQuantityByAvailabilityArray($availabilityArray, $qty);
                }

                if (!$dates->getIsBuyout()) {

                    // if not MSI we only reserve for one source so there is one item in the Array
                    if (!$this->rentalHelper->isRentalInventoryEnabled()) {
                        $howMuchToReserveBySourceArray[] = ['sourceCode' => $sourceCode, 'qty' => $qty];
                    }

                    foreach ($howMuchToReserveBySourceArray as $howMuchToReserveBySourceItem) {
                        $data = $this->getDataForUpdateOfReservationOrdersTable($dates, $item, $quoteOrOrderObject, $howMuchToReserveBySourceItem, $startDateWithTurnover, $endDateWithTurnover, $type);
                        $this->addOrUpdateReservationsOrdersTableRowFromArray($data);
                    }
                    // reset array for next loop
                    $howMuchToReserveBySourceArray = [];
                } else {
                    // if is buyout, subtract quantity from rental quantity
                    $currentQty = $this->rentalHelper->getAttribute($item->getProductId(), 'sirent_quantity');
                    $updateQty = $currentQty - $qty;
                    $this->updateSirentQuantity($item->getProductId(), $updateQty);
                }
            }
        }
    }

        private function getDataForUpdateOfReservationOrdersTable($dates, $item, $orderOrQuoteObject, $howMuchToReserveBySourceItem, $startDateWithTurnover, $endDateWithTurnover, $reserveType){
            $orderOrQuoteId = ($item->getOrderId() != null) ? $item->getOrderId() : $item->getQuoteId();
	    $data = [
                'order_id'                 => $orderOrQuoteId,
                'product_id'               => $item->getProductId(),
                'order_item_id'            => $item->getId(),
                'order_increment_id'       => $orderOrQuoteObject->getIncrementId(),
                'qty'                      => $howMuchToReserveBySourceItem['qty'],
                'not_check_valid'          => true,
                'start_date_with_turnover' => $startDateWithTurnover,
                'end_date_with_turnover'   => $endDateWithTurnover,
                'start_date_use_grid'      => $startDateWithTurnover,
                'end_date_use_grid'        => $endDateWithTurnover,
                'start_date'               => $dates->getStartDate()->format( 'Y-m-d H:i:s' ),
                'end_date'                 => $dates->getEndDate()->format( 'Y-m-d H:i:s' ),
                'source_code'              => $howMuchToReserveBySourceItem['sourceCode'],
                'order_type'               => $reserveType
            ];
            return $data;
        }


    /**
     * Cancels a quote's reserved inventory
     * reservationorders rows will be deleted
     * Inventory Arrays will be updated
     *
     *  @param  \Magento\Quote\Model\Quote $quote
     */
    public function cancelQuoteReservedQuantity( $quote )
    {
        $resorderid = $quote->getId();
        $this->searchCriteriaBuilder->addFilter( 'order_id', $resorderid );
        $this->searchCriteriaBuilder->addFilter( 'order_type', 'rfq' );
        $criteria      = $this->searchCriteriaBuilder->create();
        $resOrderItems        = $this->reservationOrdersRepository->getList( $criteria )->getItems();
        /** @var \SalesIgniter\Rental\Model\ReservationOrders $resOrder */
        foreach($resOrderItems as $resOrder)
        {
            $this->cancelReservationQty( $resOrder, $resOrder->getQtyUseGrid(), false );
            $resOrder->delete();
        }
    }


	/**
     *
     * DEPRECATED - to be removed later
     * this table seems to no longer be used
     *
	 * Function used to update the inventory with order increments array for every interval
	 * I think this function should be used for reports only and grids.
	 * Maybe should be used from the report, like and indexer. Update with order data.
	 * We never need order increments ids. Should also be a cron.
	 * The main issue is can add some not useful overhead to add it to calculation even if is per product.
	 *
	 * @param array $updatedInventory
	 * @param int   $productId
	 */
	private function updateGridTableWithData( &$updatedInventory, $productId ) {
		foreach ( $updatedInventory as $invTable ) {
			if ( $invTable['q'] > 0 ) {
				$startPeriod = new \DateTime( $invTable['s'] . ':00' );
				$endPeriod   = new \DateTime( $invTable['e'] . ':00' );
				$endPeriod->sub( new \DateInterval( 'PT1S' ) );
				$this->searchCriteriaBuilder->addFilter( 'main_table.product_id', $productId );
				$this->searchCriteriaBuilder->addFilter( 'qty_use_grid', 0, 'gt' );
				$this->searchCriteriaBuilder->addFilter( 'start_date_use_grid', $endPeriod->format( 'Y-m-d H:i:s' ), 'lteq' );
				$this->searchCriteriaBuilder->addFilter( 'end_date_use_grid', $startPeriod->format( 'Y-m-d H:i:s' ), 'gteq' );
				/*$this->searchCriteriaBuilder->addFilter('start_date_use_grid',
					new \Zend_Db_Expr('STR_TO_DATE("' . $endPeriod->format('Y-m-d H:i:s') . '", "%Y-%m-%d %H:%i:%s")'), 'le'
				);
				$this->searchCriteriaBuilder->addFilter('end_date_use_grid',
					new \Zend_Db_Expr('STR_TO_DATE("' . $startPeriod->format('Y-m-d H:i:s') . '", "%Y-%m-%d %H:%i:%s")'), 'ge'
				);*/
				//->addFilter('attribute_id', [$attributeIds['selected']], 'in');
				$criteria      = $this->searchCriteriaBuilder->create();
				$items         = $this->reservationOrdersRepository->getList( $criteria )->getItems();
				$invTable['o'] = [];
				foreach ( $items as $item ) {
					$invTable['o'] = array_unique( array_merge( $invTable['o'], [ $item->getOrderId() ] ) );
				}
				$this->inventoryGridRepository->deleteByProductId( $productId );
				$this->inventoryGridRepository->saveFromArray( [
					'start_date' => $startPeriod->format( 'Y-m-d H:i:s' ),
					'end_date'   => $endPeriod->format( 'Y-m-d H:i:s' ),
					'qty'        => $invTable['q'],
					'product_id' => $productId,
					'order_ids'  => implode( ',', $invTable['o'] ),
				] );
			}
		}
	}

	/**
	 * @param $product
	 *
	 * @return int
	 */
	public function getNonReturnedQtyForProduct( $product ) {
		$productId = $this->rentalHelper->getProductIdFromObject( $product );
		$today     = $this->calendarHelper->getTimeAccordingToTimeZone();
		$today     = new \DateTime( $today->format( 'Y-m-d H:i:s' ) );
		$qty       = 0;
		$this->searchCriteriaBuilder->addFilter( 'main_table.product_id', $productId );
		$this->searchCriteriaBuilder->addFilter( 'qty_use_grid', 0, 'gt' );

		/**
         * Only reduce if today is after the end date with turnover, if disabled causes issues with
         * non returned qty twice both from getSirentQuantity and from the reservation array
         **/
        if($this->stock->nonReturnedRentalsRemovePermanent() == false){
            $this->searchCriteriaBuilder->addFilter( 'end_date_with_turnover', $today->format( 'Y-m-d H:i:s' ), 'lt' );
        }
		$this->searchCriteriaBuilder->addFilter( 'return_date', new \Zend_Db_Expr( 'null' ), 'is' );
		$this->searchCriteriaBuilder->addFilter( 'ship_date', new \Zend_Db_Expr( 'not null' ), 'is' );

		$criteria = $this->searchCriteriaBuilder->create();
		/** @var array $items */
		$items = $this->reservationOrdersRepository->getList( $criteria )->getItems();
		foreach ( $items as $item ) {
			$qty += (int) $item->getQtyUseGrid();
		}

		return $qty;
	}

	/**
	 * @param $reservationOrder
	 * @param $currentSerialsToReturn
	 * @param $currentQtyReturn
	 * @param $item
	 */
	protected function updateSerialnumberDetailsTableToAvailable(&$reservationOrder, $currentSerialsToReturn, $currentQtyReturn, &$item ) {
		$reservationOrder->setQtyReturned( $reservationOrder->getQtyReturned() + $currentQtyReturn );

		if ( count( $currentSerialsToReturn ) > 0 ) {
			$oldSerialsReturned = explode( ',', $reservationOrder->getSerialsReturned() );
			$allSerialReturned  = array_merge( $oldSerialsReturned, $currentSerialsToReturn );
			$allSerialReturned  = array_filter( $allSerialReturned, function ( $value ) {
				return $value !== '';
			} );
			$reservationOrder->setSerialsReturned( implode( ',', $allSerialReturned ) );
			$item->setSerialsReturned( implode( ',', $currentSerialsToReturn ) );
			$this->serialNumberDetailsRepository->updateSerials( $reservationOrder->getProductId(), 'available', $currentSerialsToReturn, null );
		}
	}

	/**
     * Updates the serials in the sirental_serialnumber_details to out
     *
	 * @param $reservationOrder
	 * @param $qtyShip
	 * @param $serialsShipped
	 * @param $data
	 */
	protected function updateSerialnumberDetailsTableToOut($reservationOrder, $qtyShip, $serialsShipped, &$data ) {
		$reservationOrder->setQtyShipped( $reservationOrder->getQtyShipped() + $qtyShip );
		if(is_string($serialsShipped)){
		    $serialsShipped = explode(',', $serialsShipped);
        }
		if(is_string($serialsShipped)){
		    $serialsShipped = [$serialsShipped];
        }
		$serialsShipped = array_map('trim', $serialsShipped);
		if ( count( $serialsShipped ) > 0 ) {
			$oldSerialsShipped = explode( ',', $reservationOrder->getSerialsShipped() );
			$allSerialShipped  = array_merge( $oldSerialsShipped, $serialsShipped );
			$allSerialShipped  = array_filter( $allSerialShipped, function ( $value ) {
				return $value !== '';
			} );
			$reservationOrder->setSerialsShipped( implode( ',', $allSerialShipped ) );
			$data['serials_shipped'] = implode( ',', $serialsShipped );
			$this->serialNumberDetailsRepository->updateSerials( $reservationOrder->getProductId(), 'out', $serialsShipped, $reservationOrder->getId() );
		}
	}

	/**
	 * @param $reservationOrder
	 *
	 * @return \Magento\Framework\Api\SearchCriteria
	 */
	protected function getShippedItems( $reservationOrder ) {
		$this->searchCriteriaBuilder->addFilter( 'parent_id', $reservationOrder->getReservationorderId() );

		// Had to comment out because of partial returns, may need refactory
		//$this->searchCriteriaBuilder->addFilter( 'return_date', new \Zend_Db_Expr( 'null' ), 'is' );
		//->addFilter('attribute_id', [$attributeIds['selected']], 'in');
		$this->sortOrderBuilder->setField( 'ship_date' );
		$this->sortOrderBuilder->setAscendingDirection();
		$sortOrder = $this->sortOrderBuilder->create();
		$this->searchCriteriaBuilder->addSortOrder( $sortOrder );
		$criteria = $this->searchCriteriaBuilder->create();

		return $criteria;
	}

	/**
	 * Sirent Quantity is the total stock for a rental product without taking into account
	 * future or current reservations.
     *
     * This quantity is reduced by non-returned reservations
     * if rentals > settings > inventory > Reserve inventory until the item is returned in inventory
     * is set to yes
     *
     * It is also reduced if a reservation has no end date.
	 *
	 * @param \Magento\Catalog\Model\Product|int $product
	 *
	 * @return int
	 *
	 * @throws \Magento\Framework\Exception\LocalizedException
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 */
	public function getSirentQuantity( $product, $stockid=null, $source=null ) {

		/** @var array $productsArray */
		if ( $this->rentalHelper->isBundle( $product ) ) {
			$productsArray = $this->productHelper->getProductsSelectionsArray( $product );

			$qty = 10000;
			/*
			 * bundle has:
			 * 3XP1
			 * 5XP2
			 * 7XP3
			 * P1->current_stock = 5
			 * P2->current_stock = 3
			 * P3->current_stock = 6
			 * bundle->current_stock = 0
			 */

			foreach ( $productsArray as $selectionProduct => $iProduct ) {
				if ( is_numeric( $iProduct['selection_product_quantity'] ) && $iProduct['selection_product_quantity'] > 0 ) {
					$iProductSirentQty = $this->getSirentQuantity( $iProduct['selection_product_id'] );
					$curQty            = (int) ( $iProductSirentQty / $iProduct['selection_product_quantity'] );
					if ( $curQty < $qty ) {
						$qty = $curQty;
					}
				}
			}
		} else {
            // add way to get bundle simple products qty available? for now fake to 100 later can add real check if necessary
            if($this->rentalHelper->isRentalType($product)){

			$qty = $this->rentalHelper->getAttribute( $product, 'sirent_quantity' );

			// if using MSI return stock proper way
			if($this->rentalHelper->isRentalInventoryEnabled()){
			    $stockManager = $this->objectManager->create('SalesIgniter\Rentalinventory\Model\StockManagement');
			    if(!is_object(($product))){
                    $product = $this->productRepository->getById($product);
                }
			    if($stockid != null) {
                    $qty = $stockManager->getQuantityForProductBySkuAndStockId($product->getSku(), $stockid);
                } else if ($source != null) {
                    $qty = $stockManager->getQuantityForProductBySkuAndSources($product->getSku(), $source);
                } else {
                    $qty = $stockManager->getQuantityForProductBySkuFromWebsite($product->getSku());
                }
            }

            } else {
                $qty = 100;
            }
			if ( ! $qty ) {
				$qty = 0;
			}
			if ( $this->stock->reserveInventoryUntilReturnDate() ) {
				$qty -= $this->getNonReturnedQtyForProduct( $product );
			}
			$qty -= $this->getNonEndDateQtyForProduct( $product );
		}

		return $qty;
	}

	/**
     * The Inventory Array is stored serialized by product. We return it here as an array.
     * The Inventory Array is calculated from the Reservation Orders table in order to speed up inventory checks.
	 *
	 * @param $product
	 *
	 * @return array
	 *
	 */
	public function getInventoryArray($product, $source = null, $stockid = null ) {
		$currentInventory = [];
		if ( null === $product ) {
			return $currentInventory;
		}
		/** @var array $productsArray */
		if ( $this->rentalHelper->isBundle( $product ) ) {
			$productsArray = $this->productHelper->getProductsSelectionsArray( $product );

			/*
			 * bundle has:
			 * 3XP1
			 * 5XP2
			 * 7XP3
			 * P1->inventory_table = []
			 * P2->inventory_table = []
			 * P3->inventory_table = []
			 * bundle->current_stock = 0
			 */
			foreach ( $productsArray as $selectionProduct => $iProduct ) {
				$curInventoryTable = $this->getInventoryArray( $iProduct['selection_product_id'] );
				// We can merge inventory array for all products in bundle apparently
				$currentInventory  = array_merge( $currentInventory, $curInventoryTable );
			}
		} else {
			$inventorySerialized = $this->rentalHelper->getAttribute( $product, 'sirent_inv_bydate_serialized' );
            if($this->rentalHelper->isRentalInventoryEnabled()) {
                $stockManager = $this->objectManager->create('SalesIgniter\Rentalinventory\Model\StockManagement');
                if(!is_object(($product))){
                    $product = $this->productRepository->getById($product);
                }
                if(($source == null && $stockid == null) || $stockid != null)
                {
                    // if both are null we assume is website inventory check and will get website source code
                    $inventorySerialized = $stockManager->getSerializedInventoryArrayByStockFromStockTable($product, $stockid);
                }
                if($source != null){
                    $inventorySerialized = $stockManager->getSerializedInventoryArrayBySourceFromSourceTable($product, $source);
                }

            }
			if ( $inventorySerialized && $inventorySerialized !== '0.0000' && $inventorySerialized !== 'null' ) {
				$currentInventory = unserialize( $inventorySerialized );
			}
		}

		return $currentInventory;
	}

	/**
	 * Returns the available quantity for the start and end dates for the product
	 * Can work with reservations excluded by id from inventory.
     *
     * Also can return optionally the 'd' distribution of reservations such as:
     * 'r' => 5   means 5 rfq reservations for that time period
     * 'o' => 3   means 3 order reservations for that time period
     * Meaning how many of each reservation type were made
     *
	 *
	 * @param        $product
	 * @param string $startDate This can include turnover if necessary, it is not automatically added
	 * @param string $endDate This can include turnover if necessary, it is not automatically added
	 * @param        $excludingReservationsIds
     * @param array $sources requires Rental Inventory MSI module to function
     * @param int   $stockid requires Rental Inventory MSI module to function
	 * @param bool $returnDistribution if true return will be an array [$availableQuantity, [$distribution]]
	 * @return int|array
	 *
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 * @throws \LogicException
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	public function getAvailableQuantity( $product, $startDate = '', $endDate = '', $excludingReservationsIds = [], $sources=null, $stockid=null, $returnDistribution=false ) {
		$maxQty = $this->getSirentQuantity( $product );
		if($sources != null && $stockid != null){
            throw new \Exception('Use either Sources or Stockid, not both when getting available inventory');
        }
		if($sources != null && $this->rentalHelper->isRentalInventoryEnabled()){
            $stockManager = $this->objectManager->create('SalesIgniter\Rentalinventory\Model\StockManagement');
            $maxQty = $stockManager->getQuantityForProductBySkuAndSources($product->getSku(), $sources);
        }
        if($stockid != null && $this->rentalHelper->isRentalInventoryEnabled()){
            $stockManager = $this->objectManager->create('SalesIgniter\Rentalinventory\Model\StockManagement');
            $maxQty = $stockManager->getQuantityForProductBySkuAndStockId($product->getSku(), $stockid);
        }
		if ( $startDate === '' || $endDate === '' ) {
			return intval($maxQty);
		}
		try {
			$toCheckPeriod = new Period( $startDate, $endDate );
		} catch ( \LogicException $e ) {
			return $maxQty;
		}

		if ( ! is_array( $excludingReservationsIds ) ) {
			$excludingReservationsIds = [ $excludingReservationsIds ];
		}
		$ExcludedReservations = [];
		if ( count( $excludingReservationsIds ) > 0 ) {
			$this->searchCriteriaBuilder->addFilter( 'reservationorder_id', $excludingReservationsIds, 'in' );
			$this->searchCriteriaBuilder->addFilter( 'main_table.product_id', $this->rentalHelper->getProductIdFromObject( $product ) );
			$this->searchCriteriaBuilder->addFilter( 'qty_use_grid', 0, 'gt' );
			$criteria = $this->searchCriteriaBuilder->create();
			$ExcludedReservations    = $this->reservationOrdersRepository->getList( $criteria )->getItems();
		}

		/** @var array $currentInventory */
		$currentInventory = $this->getInventoryArray( $product, $sources, $stockid );
		$distribution = null;
		$reservedQty      = 0;
		/** @var array $reservationObject */
		foreach ( $currentInventory as $reservationObject ) {

			/** @var Period $reservationPeriod */
			$reservationPeriod    = new Period(
				$reservationObject['s'] . ':00',
				$reservationObject['e'] . ':00'
			);
			$QtyFromExcludedReservations = 0;
			foreach ( $ExcludedReservations as $item ) {
				/* @var Period $reservationPeriod */
				$reservationPeriodAvailable = new Period( $item->getStartDateUseGrid(), $item->getEndDateUseGrid() );
				if ( $reservationPeriodAvailable->overlaps( $reservationPeriod ) || $reservationPeriodAvailable->sameValueAs( $reservationPeriod ) ) {
				    // Needs checking for logic error, we need quantity available for whole rental period
                    // but here it looks like we only check if it overlaps, maybe is ok
                    // since this is mostly for adding back quantity in reservation editing
                    // to remove the original reservation from calculation
					if ( $QtyFromExcludedReservations < (int) $item->getQtyUseGrid() ) {
						$QtyFromExcludedReservations = (int) $item->getQtyUseGrid();
					}
				}
			}

			if ( $reservationPeriod->overlaps( $toCheckPeriod ) || $reservationPeriod->sameValueAs( $toCheckPeriod ) ) {

			    // for debug
//			    print 'Current inventory array count ' . count($currentInventory);
//			    print '<br />overlap found.';
//			    print '<br /> reservation period start date ' . $reservationPeriod->getStartDate()->format('Y-m-d H:i');
//                print '<br /> reservation period end date ' . $reservationPeriod->getEndDate()->format('Y-m-d H:i');
//                print '<br /> to check start date ' . $toCheckPeriod->getStartDate()->format('Y-m-d H:i');
//                print '<br /> to check end date ' . $toCheckPeriod->getEndDate()->format('Y-m-d H:i');
//                print '<br />$current reserved quantity ' . $reservedQty;
//                print '<br />' . var_dump($reservationObject);
//			    print "<br /> " . "Reserved Quantity " . $reservationObject['q'];

			    $reservationObjectQuantity = (int)$reservationObject['q'];
			    // find biggest reserved quantity within the dates
				if ( $reservedQty < $reservationObjectQuantity - $QtyFromExcludedReservations ) {
					$reservedQty = $reservationObjectQuantity - $QtyFromExcludedReservations;
					if(isset($reservationObject['d'])){
                        $distribution = $reservationObject['d'];
                    } else {
                        $distribution = null;
                    }
				}
			}
		}

		// for debug
//		print '<br />Max quantity: ' . $maxQty;
//        print '<br />Reserved quantity: ' . $reservedQty;

		$availableQuantity = ($maxQty - $reservedQty);
		if($returnDistribution){
		    return [intval($availableQuantity), $distribution];
        }
		return intval($availableQuantity);
	}

	/**
	 * Function check if selected dates have any errors.
	 *
	 * @param            $product
	 * @param            $dates
	 * @param            $quantityToCheck
	 * @param null|array $baseInventory The unserialized inventory array
	 *
	 * @return int
	 *
	 * @throws \LogicException
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	public function checkIntervalValid($product, $dates, $quantityToCheck, $baseInventory = null, $storeid=null) {
		$errorType = Stock::NO_ERROR;
		if ( $quantityToCheck === 0 ) {
			return $errorType;
		}
		$noMinimumPeriodCheck = false;
		if ( $dates->getWithoutMinimumPeriod() ) {
			$noMinimumPeriodCheck = true;
		}
		/** @var \DateTime $fromDate */
		$fromDate = $dates->getStartDate();
		/** @var \DateTime $toDate */
		$toDate = $dates->getEndDate();
		/** @var \DateTime $sendDateorStartWithTurnover */
		$sendDateorStartWithTurnover = $dates->getStartDateWithTurnover();
		/** @var \DateTime $returnDateorEndDateWithTurnover */
		$returnDateorEndDateWithTurnover            = $dates->getEndDateWithTurnover();
		$disabledDaysWeekStart = $this->calendarHelper->getDisabledDaysWeekStart( $product );
		$disabledDaysWeekEnd   = $this->calendarHelper->getDisabledDaysWeekEnd( $product );
		$disabledDates         = $this->calendarHelper->getExcludedDates( ExcludedDaysWeekFrom::CALENDAR, $product );
		$disabledDatesFull     = $this->calendarHelper->getExcludedDates( ExcludedDaysWeekFrom::FULL_CALENDAR, $product );
		$minimumPeriod         = $this->calendarHelper->getMinimumPeriod( $product );
		$maximumPeriod         = $this->calendarHelper->getMaximumPeriod( $product );
		$availableQuantity     = $this->getSirentQuantity( $product );

		// MSI mod should get available quantity by stock id
		if($this->rentalHelper->isRentalInventoryEnabled()){
            $stockManager = $this->objectManager->create('SalesIgniter\Rentalinventory\Model\StockManagement');
            if(!is_object($product)){
                $product = $this->productRepository->getById($product);
            }
            $sku = $product->getSku();
            if($this->rentalHelper->isRfqEnabled() && strpos($this->request->getRouteName(), 'amasty_quote') !== false){
                $rfqsession = $this->objectManager->get('\Amasty\RequestQuote\Model\Quote\Backend\Session');
                $storeid = $rfqsession->getStore()->getId();
            } else {
                $storeid = $this->quoteSession->getStore()->getId();
            }
            $stockid = $stockManager->getStockIdFromStoreId($storeid);
            $availableQuantity = $this->getSirentQuantity($product, $stockid);
        }
		if ( null === $baseInventory ) {
			$baseInventory = $this->getInventoryArray( $product );
		}

		$diff = $toDate->diff( $fromDate );


		// Minimum period error check
		if ( ! $noMinimumPeriodCheck &&
		     $this->dateHelper->compareInterval( '0d', $minimumPeriod ) !== 0 &&
		     $this->dateHelper->compareInterval( $diff, $minimumPeriod, true ) === - 1
		) {
			$errorType = Stock::MINIMUM_PERIOD_ERROR;

			return $errorType;
		}

		// Maximum period error check
		if ( $this->dateHelper->compareInterval( '0d', $maximumPeriod ) !== 0 && $this->dateHelper->compareInterval( $diff, $maximumPeriod, true ) === 1 ) {
			$errorType = Stock::MAXIMUM_PERIOD_ERROR;

			return $errorType;
		}

		// Enough quantity error check, skip if allow overbooking is enabled
		if ( $quantityToCheck > $availableQuantity && ! $this->calendarHelper->allowOverbooking() ) {
			$errorType = Stock::NOT_ENOUGH_QUANTITY_ERROR;
			return $errorType;
		}


        $skipQuantityCheckBecauseOrderEditTypeUsed = false;

        // Use alternate quantity check if is backend order edit, we exclude the previous order id from quantity reserved
        if(strpos( $this->request->getFullActionName(), 'sales_order_edit' ) !== false ){
            $skipQuantityCheckBecauseOrderEditTypeUsed = true;
            $orderEditIdToExclude = $this->quoteSession->getOrder()->getId();
            $this->searchCriteriaBuilder->addFilter( 'main_table.order_id', $orderEditIdToExclude );
            $criteria = $this->searchCriteriaBuilder->create();
            $items    = $this->reservationOrdersRepository->getList( $criteria )->getItems();
            $reservationIdsToExclude = [];
            foreach ( $items as $item ) {
                array_push($reservationIdsToExclude, $item->getReservationorderId());
            }
            $quantityAvailable = $this->getAvailableQuantity($product, $sendDateorStartWithTurnover, $returnDateorEndDateWithTurnover, $reservationIdsToExclude);
            if ($quantityToCheck > $quantityAvailable){
                $errorType = Stock::BOOKED_DATES_ERROR;
                return $errorType;
            };
        }

        // Makes sure there is a continuous period where full quantity is available
        if($skipQuantityCheckBecauseOrderEditTypeUsed != true) {
            $findResult = \Underscore\Types\Arrays::find($baseInventory, function ($dateElem) use ($quantityToCheck, $availableQuantity, $sendDateorStartWithTurnover, $returnDateorEndDateWithTurnover) {
                $startDateWithTurnover = new \DateTime($dateElem['s'], $sendDateorStartWithTurnover->getTimezone());
                $endDateWithTurnover = new \DateTime($dateElem['e'], $returnDateorEndDateWithTurnover->getTimezone());
                if ($quantityToCheck > -1 && $this->dateHelper->checkDatesOverlap($startDateWithTurnover, $endDateWithTurnover, $sendDateorStartWithTurnover, $returnDateorEndDateWithTurnover)) {
                    // $dateElem['q'] quantity reserved for that period
                    if ($quantityToCheck > $availableQuantity - $dateElem['q']) {
                        return true;
                    }
                }
            });

            if (null !== $findResult && $findResult !== false && !$this->calendarHelper->allowOverbooking()) {
                $errorType = Stock::BOOKED_DATES_ERROR;
                return $errorType;
            }
        }

		// Date is disabled error check for Start Date (From)
		$findResult = \Underscore\Types\Arrays::find( $disabledDates, function ( $dateElem ) use ( $fromDate ) {
			$startDate = new \DateTime( $dateElem['s'] );
			$endDate   = new \DateTime( $dateElem['e'] );
			$endDate->sub( new \DateInterval( 'PT1S' ) );
			return $this->dateHelper->isRecurringDateBetween( $startDate, $endDate, $fromDate, $dateElem['r'] );
		} );
		if ( null !== $findResult && $findResult !== false ) {
			$errorType = Stock::START_DATE_DISABLED_ERROR;
			return $errorType;
		}

		// Date is disabled error check for End Date (To)
		$findResult = \Underscore\Types\Arrays::find( $disabledDates, function ( $dateElem ) use ( $toDate ) {
			$startDate = new \DateTime( $dateElem['s'] );
			$endDate   = new \DateTime( $dateElem['e'] );
			$endDate->sub( new \DateInterval( 'PT1S' ) );
			return $this->dateHelper->isRecurringDateBetween( $startDate, $endDate, $toDate, $dateElem['r'] );
		} );
		if ( null !== $findResult && $findResult !== false ) {
			$errorType = Stock::END_DATE_DISABLED_ERROR;
			return $errorType;
		}

		// Start date error checking not sure which
		$findResult = \Underscore\Types\Arrays::find( $disabledDatesFull, function ( $dateElem ) use ( $fromDate ) {
			$newDate = new \DateTime( $dateElem['s'] );
			return $this->dateHelper->isRecurringDate( $newDate, $fromDate, $dateElem['r'] );
		} );
		if ( null !== $findResult && $findResult !== false ) {
			$errorType = Stock::START_DATE_DISABLED_ERROR_FULL;
			return $errorType;
		}

		// End date is disabled not sure which type
		$findResult = \Underscore\Types\Arrays::find( $disabledDatesFull, function ( $dateElem ) use ( $toDate ) {
			$newDate = new \DateTime( $dateElem['s'] );
			return $this->dateHelper->isRecurringDate( $newDate, $toDate, $dateElem['r'] );
		} );
		if ( null !== $findResult && $findResult !== false ) {
			$errorType = Stock::END_DATE_DISABLED_ERROR_FULL;
			return $errorType;
		}

		// Start date is disabled weekly recurring
		$findResult = \Underscore\Types\Arrays::find( $disabledDaysWeekStart, function ( $day ) use ( $fromDate ) {
			return ( $day - 1 ) === (int) $fromDate->format( 'w' );
		} );
		if ( null !== $findResult && $findResult !== false ) {
			$errorType = Stock::START_DATE_DISABLED_ERROR;
			return $errorType;
		}

		// End date is disabled weekly recurring error check
		$findResult = \Underscore\Types\Arrays::find( $disabledDaysWeekEnd, function ( $day ) use ( $toDate ) {
			return ( $day - 1 ) === (int) $toDate->format( 'w' );
		} );
		if ( null !== $findResult && $findResult !== false ) {
			$errorType = Stock::END_DATE_DISABLED_ERROR;
			return $errorType;
		}

		return $errorType;
	}

	/**
	 * Function return the first available date for the product.
	 *
	 * This is a highly intensive operation because if time increments are used along with minimum periods and hours are used
	 * is checking hour per hour to see what is the next available interval for the minimum period. There is no fast way to check
	 *
	 * @param null $product
	 * @param bool $format
	 *
	 * @return \DateTime
	 *
	 * @throws \LogicException
	 * @throws \Magento\Framework\Exception\LocalizedException
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 * @throws \RuntimeException
	 */
	public function getFirstDateAvailable( $product = null, $format = true ) {
		$todayInStoreTimezone = $this->calendarHelper->getTimeAccordingToTimeZone();

		$todayWithTimeInStoreTimezone   = $todayInStoreTimezone;
		$hoursForNextDay = $this->calendarHelper->getNextDayHour($product);
		$todayCurrentInStoreTimezoneWithTimeSetToHourForNextDay    = new \DateTime( $todayInStoreTimezone->format( 'Y-m-d' ) );
		$todayNoTime     = new \DateTime( $todayInStoreTimezone->format( 'Y-m-d' ) );
		if ( $hoursForNextDay ) {
            $todayCurrentInStoreTimezoneWithTimeSetToHourForNextDay   = new \DateTime($todayInStoreTimezone->format( 'Y-m-d' ), $todayInStoreTimezone->getTimezone());
            $todayNoTime    = new \DateTime($todayInStoreTimezone->format( 'Y-m-d' ), $todayInStoreTimezone->getTimezone());
			$todayCurrentInStoreTimezoneWithTimeSetToHourForNextDay->setTime( (int)$hoursForNextDay[0], (int)$hoursForNextDay[1], (int)$hoursForNextDay[2] );
		} else {
			$todayCurrentInStoreTimezoneWithTimeSetToHourForNextDay->setTime( 23, 59, 59 );
		}
		$todayInStoreTimezone = $todayNoTime;

		// If we are already past the hour for next day, add 1 day to start date for checking
		if ( $this->dateHelper->compareDateTimeObj( $todayWithTimeInStoreTimezone, $todayCurrentInStoreTimezoneWithTimeSetToHourForNextDay, true ) >= 0 ) {
			$todayInStoreTimezone = $todayNoTime->add( new \DateInterval( 'P1D' ) );
		}

		// modified to show padding for bundles too, before was just simple rental type
        // may need to modify to not check turnover if bundles load slowly
		if ( $product != null && !$this->rentalHelper->isRentalType( $product ) ) {
			return $todayInStoreTimezone->format( 'Y-m-d' );
		}

		$turnoverBefore = $this->calendarHelper->getTurnoverBefore( $product );
		$padding        = $this->calendarHelper->getPadding( $product );
		/** @var Period $periodFirstDateAvailable */
		$periodFirstDateAvailable = Period::createFromDuration( $todayInStoreTimezone, 60 );

		// If padding is greater than or equal to one day, move first date available ahead by padding
		if ( $this->dateHelper->compareInterval( $padding, '1d' ) > - 1 ) {
            $dateInterval = $this->dateHelper->normalizeInterval($padding);
            $periodFirstDateAvailable = $periodFirstDateAvailable->move($dateInterval);
        }

		// If turnover is greater than padding, also move ahead the difference between them (Turnover - Padding)
        if ( $this->dateHelper->compareInterval( $turnoverBefore, $padding ) === 1 ) {
				$diffPaddingTurnoverInMinutes = $this->calendarHelper->stringPeriodToMinutes( $turnoverBefore ) -
				                             $this->calendarHelper->stringPeriodToMinutes( $padding );
				$periodFirstDateAvailable               = $periodFirstDateAvailable->move( 60 * $diffPaddingTurnoverInMinutes );
        }


        $iVal  = 0;
        $dates = new DataObject();

		if($product == null){
		    return $periodFirstDateAvailable->getStartDate()->format( 'Y-m-d' );
        }

        /**
         * Loop adding 1 day to $periodFirstDateAvailable until checkIntervalValid
         */
		while ( true ) {
			if ( $format === false && $this->calendarHelper->useTimes( $product ) ) {
				$periodFirstDateAvailable = $this->getFirstTimeAvailable( $product, false, $periodFirstDateAvailable->getStartDate() );
			}

			$dates->setStartDate( $periodFirstDateAvailable->getStartDate() );
			$dates->setEndDate( $periodFirstDateAvailable->getEndDate() );
			$dates->setStartDateWithTurnover( $this->calendarHelper->getActualTurnoverBeforeDate( $product, $periodFirstDateAvailable->getStartDate() ) );
			$dates->setEndDateWithTurnover( $this->calendarHelper->getActualTurnoverAfterDate( $product, $periodFirstDateAvailable->getEndDate() ) );
			$dates->setWithoutMinimumPeriod( true );
			$checkInterval = $this->checkIntervalValid( $product, $dates, 1 );
			// Break when No Errors $checkInterval == 0
			if ( $checkInterval !== Stock::NO_ERROR && $checkInterval !== Stock::END_DATE_DISABLED_ERROR && $checkInterval !== Stock::START_DATE_DISABLED_ERROR ) {
				$dateInterval = $this->dateHelper->normalizeInterval( '1d' );
				$periodFirstDateAvailable  = $periodFirstDateAvailable->move( $dateInterval );
			} else {
				break;
			}
			++ $iVal;
			if ( $iVal > 200 ) {
				$logger = \Magento\Framework\App\ObjectManager::getInstance()->get( 'Magento\Framework\Logger\Monolog' );
				$logger->pushHandler( new \Monolog\Handler\StreamHandler( BP . '/var/log/pprlog.log' ) );
				$logger->addDebug( 'Might be infinite loop stock class ' );
				break;
			}
		}
		if ( $format ) {
			return $periodFirstDateAvailable->getStartDate()->format( 'Y-m-d' );
		} else {
			return $periodFirstDateAvailable;
		}
	}

	/**
	 * Function return the first available time for the first available date.
	 *
	 * todo should take into account store hours to be more specific
	 *
	 * @param null           $product
	 * @param bool           $format
	 * @param \DateTime|null $today
	 *
	 * @return int
	 *
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 * @throws \LogicException
	 * @throws \Magento\Framework\Exception\LocalizedException
	 * @throws \RuntimeException
	 */
	public function getFirstTimeAvailable( $product = null, $format = true, $today = null ) {
		$currentDay = $this->calendarHelper->getTimeAccordingToTimeZone();
		$currentDay = new \DateTime( $currentDay->format( 'Y-m-d H:i:s' ) );

		if ( $today === null || ! $this->rentalHelper->isRentalTypeSimple( $product ) || $this->dateHelper->compareDateTimeObj( $today, $currentDay, false ) === 0 ) {
			$today = $currentDay;
		}

		if ( null === $product ) {
			return $today->format( 'Y-m-d' );
		}
		$turnoverBefore           = $this->calendarHelper->getTurnoverBefore( $product );
		$padding                  = $this->calendarHelper->getPadding( $product );
		$minimumPeriod            = $this->calendarHelper->getMinimumPeriod( $product );
		$minimumDurationInSeconds = 60;
		if ( $format === false ) {
			if ( $this->dateHelper->compareInterval( $minimumPeriod, '0d' ) !== 0 ) {
				$minimumDurationInSeconds = $this->calendarHelper->stringPeriodToMinutes( $minimumPeriod ) * 60 + 60;
			}
			if ( $minimumDurationInSeconds < 60 * $this->calendarHelper->timeIncrement() ) {
				$minimumDurationInSeconds = 60 * $this->calendarHelper->timeIncrement();
			}
		}
		/** @var Period $periodToday */
		$periodToday = Period::createFromDuration( $today, $minimumDurationInSeconds );

		$minimumDurationInSecondsForTimes = 60 * $this->calendarHelper->timeIncrement();
		if ( $this->dateHelper->compareInterval( $padding, '1d' ) === - 1 ) {
			$dateInterval = $this->dateHelper->normalizeInterval( $padding );
			$periodToday  = $periodToday->move( $dateInterval );
			if ( $this->dateHelper->compareInterval( $turnoverBefore, $padding ) === 1 && $this->dateHelper->compareInterval( $turnoverBefore, '1d' ) === - 1 ) {
				$differencePaddingTurnover = $this->calendarHelper->stringPeriodToMinutes( $turnoverBefore ) -
				                             $this->calendarHelper->stringPeriodToMinutes( $padding );
				$periodToday               = $periodToday->move( 60 * $differencePaddingTurnover );
			}
		}

		$iVal  = 0;
		$dates = new DataObject();

		while ( true ) {
			$dates->setStartDate( $periodToday->getStartDate() );
			$dates->setEndDate( $periodToday->getEndDate() );
			$dates->setStartDateWithTurnover( $this->calendarHelper->getActualTurnoverBeforeDate( $product, $periodToday->getStartDate() ) );
			$dates->setEndDateWithTurnover( $this->calendarHelper->getActualTurnoverAfterDate( $product, $periodToday->getEndDate() ) );
			$checkInterval = $this->checkIntervalValid( $product, $dates, 1 );
			if ( $checkInterval !== Stock::NO_ERROR && $checkInterval !== Stock::END_DATE_DISABLED_ERROR && $checkInterval !== Stock::START_DATE_DISABLED_ERROR ) {
				$periodToday = $periodToday->move( $minimumDurationInSecondsForTimes );
			} else {
				break;
			}

			if ( $format === true && $this->dateHelper->compareDateTimeObj( $today, $periodToday->getStartDate(), false ) !== 0 ) {
				break;
			}
			++ $iVal;
			if ( $iVal > 10000 ) {
				$logger = \Magento\Framework\App\ObjectManager::getInstance()->get( 'Magento\Framework\Logger\Monolog' );
				$logger->pushHandler( new \Monolog\Handler\StreamHandler( BP . '/var/log/pprlog.log' ) );
				$logger->addDebug( 'Might be infinite loop stock class ' );
				break;
			}
		}
		if ( $format ) {
			if ( $this->calendarHelper->timeTypeAmpm() ) {
				return $periodToday->getStartDate()->format( 'h:i a' );
			} else {
				return $periodToday->getStartDate()->format( 'H:i' );
			}
		} else {
			return $periodToday;
		}
	}


    /**
     * Creates or Updates an Inventory Array field
     *
     * @param $startDateWithTurnover *if exists should be start date use grid
     * @param $endDateWithTurnover *if exists should be end date use grid
     * @param $newReservationQty *can be negative to indicate quantity to cancel
     * @param $currentInventory *can be empty
     * @param $type *Refers to if is order "o" or rfq "r" or maintenance 'm' or manual 'n'
     * @return array
     */

    public function createOrUpdateInventoryArray($startDateWithTurnover, $endDateWithTurnover, $newReservationQty, $currentInventory=null, $type="o")
    {
        $newReservationQty = (int) $newReservationQty;
        /** @var array $updatedInventory */
        $updatedInventory = [];
        /** @var Period $newReservationBeingAddedPeriod */
        // Should use start date use grid and end date use grid if exists
        $newReservationBeingAddedPeriod = new Period( $startDateWithTurnover, $endDateWithTurnover );
        /** @var array $currentInventory */

        /**
         * Case if no current inventory just add new reservation to Updated Inventory array
         * (Other cases will not match since no current inventory array exists)
         */
        if ( $currentInventory == null ||
            (is_array($currentInventory) && count( $currentInventory ) === 0 )) {
            /**
             * Inventory is formed from an array like
             * 'q' -> quantity
             * 'd' -> distribution array representing different types of reservations like order "o" or rfq "r"
             * 's' -> start date
             * 'e' -> end date
             * 's'-> 'e' represents the interval where quantity is 'q'
             * we have an array of intervals and we intersect them. The intersection will increase the qty reserved.
             * And the difference will keep the same qty.
             */

            $updatedInventory[] = [
                'q' => $newReservationQty,
                'd' => [$type => $newReservationQty],
                's' => $newReservationBeingAddedPeriod->getStartDate()->format( 'Y-m-d H:i' ),
                'e' => $newReservationBeingAddedPeriod->getEndDate()->format( 'Y-m-d H:i' ),
            ];
        }
        $overlaps = false;

        if(is_array($currentInventory) && count( $currentInventory ) > 0) {
            /** @var array $oldReservationObject */
            foreach ($currentInventory as $oldReservationObject) {

                /** @var Period $oldReservationPeriod */
                $oldReservationPeriod = new Period(
                    $oldReservationObject['s'] . ':00',
                    $oldReservationObject['e'] . ':00'
                );

                if ($oldReservationPeriod->overlaps($newReservationBeingAddedPeriod)) {
                    /**
                     * We first intersect the periods if they overlaps
                     * This means that on the intersected periods the qty will increase
                     */
                    /** @var Period $intersectionPeriod */
                    $intersectionPeriod = $oldReservationPeriod->intersect($newReservationBeingAddedPeriod);

                    /**
                     * example of "d" distribution:
                     * ['o' => 5], ['r' => '3']
                     */

                    // handle case where there is no distribution
                    if(!isset($oldReservationObject['d'])){
                        $oldReservationObject['d'] = ['o' => $oldReservationObject['q']];
                    }

                    $toAddToUpdatedInventoryItem = [
                        'q' => intval($oldReservationObject['q'] + $newReservationQty),
                        'd' => $this->addQtyToDistribution($type, $newReservationQty, $oldReservationObject['d']),
                        's' => $intersectionPeriod->getStartDate()->format('Y-m-d H:i'),
                        'e' => $intersectionPeriod->getEndDate()->format('Y-m-d H:i'),
                    ];

                    /**
                     * If there was already a matching $updatedInventory period, remove it
                     * because it would only contain the new quantity, but we want old + new quantity added together
                     */
                    foreach($updatedInventory as $updatedInventoryKey => $updatedInventoryItem){
                        if($updatedInventoryItem['s'] == $toAddToUpdatedInventoryItem['s'] && $updatedInventoryItem['e'] == $toAddToUpdatedInventoryItem['e']){
                            unset($updatedInventory[$updatedInventoryKey]);
                        }
                    }

                    array_push($updatedInventory, $toAddToUpdatedInventoryItem);

                    /**
                     * Diff period for old reservation where no overlap
                     */
                    $oldReservationDiffPeriods = $oldReservationPeriod->diff($intersectionPeriod);
                    if (count($oldReservationDiffPeriods) > 0) {
                        foreach ($oldReservationDiffPeriods as $oldReservationDiffPeriod) {
                            // handle case where there is no distribution
                            if(!isset($oldReservationObject['d'])){
                                $oldReservationObject['d'] = ['o' => $oldReservationObject['q']];
                            }
                            $updatedInventory[] = [
                                'q' => intval($oldReservationObject['q']),
                                'd' => $oldReservationObject['d'],
                                's' => $oldReservationDiffPeriod->getStartDate()->format('Y-m-d H:i'),
                                'e' => $oldReservationDiffPeriod->getEndDate()->format('Y-m-d H:i'),
                            ];
                        }
                    }

                    /**
                     * Diff period for new reservation where no overlap
                     */
                    $newReservationDiffPeriods = $newReservationBeingAddedPeriod->diff($intersectionPeriod);
                    if (count($newReservationDiffPeriods) > 0) {
                        foreach ($newReservationDiffPeriods as $newReservationDiffPeriod) {
                            $updatedInventory[] = [
                                'q' => $newReservationQty,
                                'd' => [$type => $newReservationQty],
                                's' => $newReservationDiffPeriod->getStartDate()->format('Y-m-d H:i'),
                                'e' => $newReservationDiffPeriod->getEndDate()->format('Y-m-d H:i'),
                            ];
                        }
                    }
                    $overlaps = true;

                    /**
                     * No overlap, add old reservation back to new Updated Inventory Array
                     */
                } else {
                    // handle case where there is no distribution
                    if(!isset($oldReservationObject['d'])){
                        $oldReservationObject['d'] = ['o' => $oldReservationObject['q']];
                    }
                    $updatedInventory[] = [
                        'q' => intval($oldReservationObject['q']),
                        'd' => $oldReservationObject['d'],
                        's' => $oldReservationPeriod->getStartDate()->format('Y-m-d H:i'),
                        'e' => $oldReservationPeriod->getEndDate()->format('Y-m-d H:i'),
                    ];
                }
            }
        }

        /**
         * Case where there is current inventory, but none overlap with new reservation
         * so new reservation was not yet added in
         */
        if ( !$overlaps && is_array($currentInventory) && count( $currentInventory ) > 0) {
            $updatedInventory[] = [
                'q' => $newReservationQty,
                'd' => [$type => $newReservationQty],
                's' => $newReservationBeingAddedPeriod->getStartDate()->format( 'Y-m-d H:i' ),
                'e' => $newReservationBeingAddedPeriod->getEndDate()->format( 'Y-m-d H:i' ),
            ];
        }
        return $updatedInventory;
    }

    /**
     * Adds more quantity to the specified distribution type
     * like 'o', 2, array currentDistributions ['r'=>1,'o'=>2]
     * would increase the 'o' to 4
     *
     * @param $type *like 'o' for order or 'r' for rfq
     * @param $qty *qty to be added
     * @param $currentDistribution
     * @return mixed
     */
    public function addQtyToDistribution($type, $qty, $currentDistributions)
    {
        $updatedDistributions = $currentDistributions;
        $match = false;
        foreach($updatedDistributions as $distributionType => $distributionValue){
            if($type == $distributionType){
                $match = true;
                $updatedDistributions[$distributionType] = intval($distributionValue + $qty);
            }
        }
        if($match == false){
            $updatedDistributions[$type] = intval($qty);
        }
        return $updatedDistributions;
    }


    /**
     * Combines a New reservation with the Existing reservations for the Inventory Array and returns the updated Inventory Array
     *
     * The updated Inventory Array will still need to be updated to the Serialized Inventory field, this function
     * does not handle that part.
     *
     * For Maintenance when no start end dates should be a plugin to getSirentQuantity to subtract 1.
     *
     * @param int $productId
     * @param \DateTime $startDateWithTurnover
     * @param \DateTime $endDateWithTurnover
     * @param int $qty
     * @param int $qtyCancel
     * @param null $baseInventory
     * @param $type *Refers to if is order "o" or rfq "r" or maintenance 'm' or manual 'l'
     * @return array
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \LogicException
     * @throws NoSuchEntityException
     */

	public function getUpdatedInventoryArray($productId, $startDateWithTurnover, $endDateWithTurnover, $qty, $qtyCancel = 0, $baseInventory = null, $type='o') {
		try {
			$product = $this->productRepository->getById( $productId );
		} catch ( NoSuchEntityException $e ) {
			return [];
		}
		if ( $qtyCancel > 0 ) {
			$qty = - $qtyCancel;
		}
		$qty              = (int) $qty;
		$currentInventory = $baseInventory;
		if ( null === $baseInventory ) {
			$currentInventory = $this->getInventoryArray( $product );
		}
		if ( empty( $endDateWithTurnover ) || $endDateWithTurnover === '0000-00-00 00:00:00' ) {
			return $currentInventory;
		}

		$updatedInventory = $this->createOrUpdateInventoryArray($startDateWithTurnover, $endDateWithTurnover, $qty, $currentInventory, $type);
		$updatedInventory = $this->stock->normalizeInventory( $updatedInventory );
		$updatedInventory = $this->stock->compactInventory( $updatedInventory );

		return $updatedInventory;
	}

	private function getNonEndDateQtyForProduct( $product ) {
		$productId = $this->rentalHelper->getProductIdFromObject( $product );
		$qty       = 0;
		$this->searchCriteriaBuilder->addFilter( 'main_table.product_id', $productId );
		$this->searchCriteriaBuilder->addFilter( 'qty_use_grid', 0, 'gt' );
		$this->searchCriteriaBuilder->addFilter( 'end_date_with_turnover', '0000-00-00 00:00:00', 'eq' );

		$criteria = $this->searchCriteriaBuilder->create();
		/** @var array $items */
		$items = $this->reservationOrdersRepository->getList( $criteria )->getItems();
		foreach ( $items as $item ) {
			$qty += (int) $item->getQtyUseGrid();
		}

		return $qty;
	}

	/**
	 * @param \Magento\Sales\Api\Data\OrderItemInterface $item
	 * @param \Magento\Sales\Api\Data\OrderInterface     $order
	 *
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	private function reserveDisableShipping( $item, $order ) {
		if ( $item->getIsVirtual() ) {
			$item->setIsVirtual( 0 );
			$item->setFreeShipping( 1 );
			$this->orderItemRepository->save( $item );
		}
		if ( $item->getParentItem() && $item->getParentItem()->getIsVirtual() ) {
			$item->getParentItem()->setIsVirtual( 0 );
			$item->getParentItem()->setFreeShipping( 1 );
			$this->orderItemRepository->save( $item->getParentItem() );
		}
	}


}
