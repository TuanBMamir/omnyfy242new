<?php

namespace SalesIgniter\Rental\Controller\Ajax;
use SalesIgniter\Rental\Cron\AsyncInventoryUpdate;



class UpdateInventory extends \Magento\Framework\App\Action\Action
{

    private $asyncInventoryUpdate;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        AsyncInventoryUpdate $asyncInventoryUpdate
    )
    {
        parent::__construct($context);
        $this->asyncInventoryUpdate = $asyncInventoryUpdate;
    }

    public function execute()
    {
        $this->asyncInventoryUpdate->execute();
        print "Rental inventory updated";
        exit;
    }
}
