<?php

namespace SalesIgniter\Rental\Controller\Adminhtml\Report\Inventory;

use Magento\Backend\App\Action as BackendAction;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Collection as DataCollection;
use Magento\Framework\DataObject\Factory as DataObjectFactory;
use Magento\Sales\Model\OrderFactory;
use SalesIgniter\Rental\Helper\Report as ReportHelper;
use SalesIgniter\Rental\Helper\Data;
use SalesIgniter\Rental\Model\StockManagement;

class GetDateReportData extends BackendAction
{

    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var DataCollection
     */
    protected $_collection;

    /**
     * @var DataObjectFactory
     */
    protected $_dataObjectFactory;

    /**
     * @var ResourceConnection
     */
    protected $_resourceConnection;

    /**
     * @var ReportHelper
     */
    protected $_reportHelper;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;
    /**
     * @var \SalesIgniter\Rental\Block\Adminhtml\Widget\DataTable
     */
    private $gridBlock;
    /**
     * @var Data
     */
    private $rentalHelper;

    /**
     * @var StockManagement
     */
    private $stockManagement;

    /**
     * GetProductDateReportData constructor.
     *
     * @param Context                                               $context
     * @param JsonFactory                                           $resultJsonFactory
     * @param DataObjectFactory                                     $ObjectFactory
     * @param DataCollection                                        $Collection
     * @param ResourceConnection                                    $ResourceConnection
     * @param ProductFactory                                        $ProductFactory
     * @param \Magento\Sales\Model\OrderFactory                     $OrderFactory
     * @param \SalesIgniter\Rental\Helper\Report                    $ReportHelper
     * @param \SalesIgniter\Rental\Block\Adminhtml\Widget\DataTable $gridBlock
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        DataObjectFactory $ObjectFactory,
        DataCollection $Collection,
        ResourceConnection $ResourceConnection,
        ProductFactory $ProductFactory,
        OrderFactory $OrderFactory,
        ReportHelper $ReportHelper,
        \SalesIgniter\Rental\Block\Adminhtml\Widget\DataTable $gridBlock,
        Data $rentalHelper,
        StockManagement $stockManagement
    ) {
        parent::__construct($context);

        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_collection = $Collection;
        $this->_dataObjectFactory = $ObjectFactory;
        $this->_resourceConnection = $ResourceConnection;
        $this->_productFactory = $ProductFactory;
        $this->_orderFactory = $OrderFactory;
        $this->_reportHelper = $ReportHelper;
        $this->gridBlock = $gridBlock;
        $this->rentalHelper = $rentalHelper;
        $this->stockManagement = $stockManagement;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     * @throws \DomainException
     */
    public function execute()
    {
        $RequestParams = $this->getRequest()->getParams();
        $ResultJson = $this->_resultJsonFactory->create();

        $Product = $this->_productFactory
            ->create()
            ->load($RequestParams['product']);

        $Connection = $this->_resourceConnection->getConnection(ResourceConnection::DEFAULT_CONNECTION);

        $FromDate = $RequestParams['dateFrom'];
        $ToDate = $RequestParams['dateTo'];
        $displayby = isset($RequestParams['displayby']) ? $RequestParams['displayby'] : '';
        $stock = isset($RequestParams['stock']) ? $RequestParams['stock'] : '';
        $source = isset($RequestParams['source']) ? $RequestParams['source'] : '';

        /**
         * Get reservations as a collection
         */
        $ReservationsCollection = $this->_reportHelper->getRentalOrders([
            'use_turnover_date' => true,
            'start_date' => $FromDate,
            'end_date' => $ToDate,
            'displayby' => $displayby,
            'stock' => $stock,
            'source' => $source,
            'conditions' => [
                'product_id' => ['eq' => $Product->getId()],
            ]
        ]);

        /**
         * Convert each item of the collection to a dataobject using Magento\Framework\DataObject\Factory
         */
        foreach ($ReservationsCollection as $ReservationItem) {
            $orderid = (int)$ReservationItem['order_id'];
            $Order = $this->_orderFactory->create()->load($orderid);
            $DataObject = $this->_dataObjectFactory->create();
            $DataObject->setData($ReservationItem);

            // add RFQ data here
            if($ReservationItem['order_type'] == 'rfq' && $this->rentalHelper->isRfqEnabled()){

                /** @var \Amasty\RequestQuote\Api\QuoteRepositoryInterface $quoteRepo */
                $quoteRepo = $this->_objectManager->create('Amasty\RequestQuote\Api\QuoteRepositoryInterface');
                $quote = $quoteRepo->get($orderid,['*']);
                $DataObject->setOrderIncrementId($quote->getIncrementId());
                $DataObject->setCustomerId($quote->getCustomerId());
                $DataObject->setCustomerName($quote->getCustomerName());
            } else {
                $DataObject->setOrderIncrementId($Order->getIncrementId());
                $DataObject->setCustomerId($Order->getCustomerId());
                $DataObject->setCustomerName($Order->getCustomerName());
            }

            /**
             * Add each $dataobject to a collection so that we can make a grid with the collection
             */
            $this->_collection->addItem($DataObject);
        }

        $this->gridBlock->setCollection($this->_collection);
        $this->gridBlock->setGridUrl($this->getUrl('*/*/*', ['_current' => true]));

        if(isset($ReservationItem)) {
//            if ($ReservationItem['order_type'] == 'rfq') {
//                $orderOrRFQLink = '<a href="' . $this->getUrl('amasty_quote/quote/view', ['quote_id' => $orderid]) . '" target="_blank">RFQ #{{order_increment_id}}</a>';
//            } else {
//                $orderOrRFQLink = '<a href="' . $this->getUrl('sales/order/view', ['order_id' => $orderid]) . '" target="_blank">#{{order_increment_id}}</a>';
//            }


            $this->gridBlock->addColumn('order_increment_id', [
                'header' => __('Order or RFQ #'),
                'index' => 'order_increment_id',
                'renderer' => 'SalesIgniter\Rental\Block\Adminhtml\Widget\DataTable\Renderer\OrderIncrement',
                'renderConfig' => [
                    'template' => ''
                ]
            ]);

//            if ($DataObject->getCustomerId()) {
//            $templateCustomer = '<a href="' . $this->getUrl('customer/index/edit', ['id' => $DataObject->getCustomerId()]) . '" target="_blank">' . $DataObject->getCustomerName() . '</a>';
//            } else {
//                $templateCustomer = $DataObject->getCustomerName();
//            }

        $this->gridBlock->addColumn('customer_name', [
            'header' => __('Customer'),
            'index' => 'customer_name',
            'renderer' => 'SalesIgniter\Rental\Block\Adminhtml\Widget\DataTable\Renderer\CustomerName',
            'renderConfig' => [
                'template' => ''
            ]
        ]);

        $this->gridBlock->addColumn('start_date_with_turnover', [
            'header' => __('Start Date W/Turnover'),
            'index' => 'start_date_with_turnover',
            'timezone' => false,
            'gmtoffset' => false,
            'type' => 'datetime'
        ]);

        $this->gridBlock->addColumn('start_date', [
            'header' => __('Start Date'),
            'index' => 'start_date',
            'timezone' => false,
            'gmtoffset' => false,
            'type' => 'datetime'
        ]);

        $this->gridBlock->addColumn('end_date', [
            'header' => __('End Date'),
            'index' => 'end_date',
            'timezone' => false,
            'gmtoffset' => false,
            'type' => 'datetime'
        ]);

        $this->gridBlock->addColumn('end_date_with_turnover', [
            'header' => __('End Date W/Turnover'),
            'index' => 'end_date_with_turnover',
            'timezone' => false,
            'gmtoffset' => false,
            'type' => 'datetime'
        ]);

        $this->gridBlock->addColumn('inventory', [
            'header' => __('Inventory'),
            'index' => 'qty',
            'renderer' => 'SalesIgniter\Rental\Block\Adminhtml\Widget\DataTable\Renderer\Buildhtml',
            'renderConfig' => [
                'template' => '<ul style="list-style:none;margin:0;padding:0;width:87px">
					<li>
						<div style="min-width: 65px;width:75%;float:left;">Reserved: </div>
						<div style="width:25%;float:left;">{{qty}}</div>
					</li>
					<li>
						<div style="min-width: 65px;width:75%;float:left;">Canceled: </div>
						<div style="width:25%;float:left;">{{qty_cancel}}</div>
					</li>
					<li>
						<div style="min-width: 65px;width:75%;float:left;">Shipped: </div>
						<div style="width:25%;float:left;">{{qty_shipped}}</div>
					</li>
					<li>
						<div style="min-width: 65px;width:75%;float:left;">Returned: </div>
						<div style="width:25%;float:left;">{{qty_returned}}</div>
					</li>
				</ul><div style="clear:both;display:table;"></div>'
            ]
        ]);

        if($this->rentalHelper->isRentalInventoryEnabled()){
            $this->gridBlock->addColumn('source_code', [
                'header' => __('Source'),
                'index' => 'source_code',
                'type' => 'text'
            ]);
        }

        }

        $html = $this->gridBlock->getGridBlock()->toHtml();

        if($this->rentalHelper->isRentalInventoryEnabled()){
            $addSourcesBlock = $this->_objectManager->get('SalesIgniter\Rentalinventory\Block\Adminhtml\InventoryReport');
            $addSourcesData = $addSourcesBlock->getSourcesInfo($Product, $FromDate, $ToDate, $displayby, $source, $stock);
            $html = $addSourcesData . $html;
        } else {
            $addTotalsData = $this->getTotals($Product, $FromDate, $ToDate);
            $html = $addTotalsData . $html;
        }


        $ResultJson->setData([
            'success' => true,
            'title' => $Product->getName(),
            'html' => $html]);

        return $ResultJson;
    }

    private function getTotals($product, $startdate, $enddate)
    {
        $total = $this->stockManagement->getSirentQuantity($product);
        $availableArray = $this->stockManagement->getAvailableQuantity($product, $startdate, $enddate, [], null, null, true);
        $available = $availableArray[0];
        $distribution = $availableArray[1];
        $reservedOrder = isset($distribution['o']) ? $distribution['o'] : '';
        $reservedMaintenance = isset($distribution['m']) ? $distribution['m'] : '';
        $reservedManual = isset($distribution['l']) ? $distribution['l'] : '';
        $reservedRfq = isset($distribution['r']) ? $distribution['r'] : '';
        $html = '<table class="data-grid"><thead><tr><th>' . __('Total Qty') . '</th><th>' . __('Reserved Order') . '</th><th>' . __('Reserved Manually') . '</th><th>' . __('Reserved Maintenance') . '</th><th>' . __('Reserved RFQ') . '</th><th>' . __('Available') . '</th></tr></thead>';
        $html .= "<tr><td>{$total}</td><td>{$reservedOrder}</td><td>{$reservedManual}</td><td>{$reservedMaintenance}</td><td>{$reservedRfq}</td><td>{$available}</td></tr>";
        $html .= '</table>';
        return $html;
        // return Total / Reserved Order / Reserved RFQ / Available
    }
}
