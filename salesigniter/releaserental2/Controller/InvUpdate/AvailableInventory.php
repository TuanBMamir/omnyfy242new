<?php

namespace SalesIgniter\Rental\Controller\InvUpdate;

use Magento\Framework\UrlInterface as UrlBuilder;

/**
 * Used for debugging inventory serialized field
 *
 * Class ProductInventory
 * @package SalesIgniter\Rental\Controller\InvUpdate
 */

class AvailableInventory extends \Magento\Framework\App\Action\Action
{

    private $asyncInventoryUpdate;

    private $stockManagement;

    private $request;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \SalesIgniter\Rental\Model\StockManagement $stockManagement,
        \Magento\Framework\App\Request\Http $request,
        UrlBuilder $urlBuilder
    )
    {
        parent::__construct($context);
        $this->request = $request;
        $this->stockManagement = $stockManagement;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Returns easy to read array of product inventory from the reservations orders table
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     *
     * To use go to url: site.com/salesigniter_rental/InvUpdate/AvailableInventory/product_id/[product_id]
     *
     */
    public function execute()
    {
        $productid = $this->request->getParam('product_id');
        // $inventory = $this->stockManagement->getInventoryArray($productid);
        $startdate = '2021-04-25 00:00';
        $enddate = '2021-04-25 23:59';
        $inventory = $this->stockManagement->getAvailableQuantity($productid, $startdate, $enddate);
        function prettyPrint($a, $t='pre') {echo "<$t>".print_r($a,1)."</$t>";}
        prettyPrint($inventory);
        exit;
    }


}
