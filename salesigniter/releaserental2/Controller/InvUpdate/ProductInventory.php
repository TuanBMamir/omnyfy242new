<?php

namespace SalesIgniter\Rental\Controller\InvUpdate;

use Magento\Framework\UrlInterface as UrlBuilder;

/**
 * Used for debugging inventory serialized field
 *
 * Class ProductInventory
 * @package SalesIgniter\Rental\Controller\InvUpdate
 */

class ProductInventory extends \Magento\Framework\App\Action\Action
{

    private $asyncInventoryUpdate;

    private $stockManagement;

    private $request;

    /**
     * @var \SalesIgniter\Rental\Model\Product\Stock
     */
    private $stock;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \SalesIgniter\Rental\Model\StockManagement $stockManagement,
        \Magento\Framework\App\Request\Http $request,
        \SalesIgniter\Rental\Model\Product\Stock $stock,
        UrlBuilder $urlBuilder
    )
    {
        parent::__construct($context);
        $this->request = $request;
        $this->stockManagement = $stockManagement;
        $this->stock = $stock;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Returns easy to read array of product inventory from the reservations orders table
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     *
     * To use go to url: site.com/salesigniter_rental/InvUpdate/ProductInventory/product_id/[product_id]
     *
     */
    public function execute()
    {
        $productid = $this->request->getParam('product_id');
        // $inventory = $this->stockManagement->getInventoryArray($productid);
        $inventory = $this->stockManagement->getInventoryArray($productid);
        $inventory = $this->stock->compactInventory($inventory);
        $inventory = $this->stock->normalizeInventory($inventory);
        echo "<h3>Product Array From Serialized Field in DB</h3>";
        function prettyPrint($a, $t='pre') {echo "<$t>".print_r($a,1)."</$t>";}
        prettyPrint($inventory);
        $inventory = $this->stockManagement->getInventoryArrayFromScratch($productid);
        $inventory = $this->stock->compactInventory($inventory);
        $inventory = $this->stock->normalizeInventory($inventory);
        echo "<h3>Product Array From Reservations Orders Table Generated from Scratch</h3>";
        prettyPrint($inventory);
        exit;
    }


}
