<?php

namespace SalesIgniter\Rental\Controller\InvUpdate;

use Magento\Framework\UrlInterface as UrlBuilder;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use SalesIgniter\Rental\Helper\Data;

/**
 * Used for debugging inventory serialized field
 *
 * Class ProductInventory
 * @package SalesIgniter\Rental\Controller\InvUpdate
 */

class UpdateAllProducts extends \Magento\Framework\App\Action\Action
{

    private $asyncInventoryUpdate;

    private $stockManagement;

    private $request;

    /**
     * @var \SalesIgniter\Rental\Model\Product\Stock
     */
    private $stock;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    private $productRepository;

    public function __construct(
        Data $rentalHelper,
        ProductRepository $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\App\Action\Context $context,
        \SalesIgniter\Rental\Model\StockManagement $stockManagement,
        \Magento\Framework\App\Request\Http $request,
        \SalesIgniter\Rental\Model\Product\Stock $stock,
        UrlBuilder $urlBuilder
    )
    {
        parent::__construct($context);
        $this->request = $request;
        $this->rentalHelper = $rentalHelper;
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->stockManagement = $stockManagement;
        $this->stock = $stock;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Updates Product Inventory From Table
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     *
     * To use go to url: site.com/salesigniter_rental/InvUpdate/UpdateAllProducts/
     *
     */
    public function execute()
    {
        function prettyPrint($a, $t = 'pre')
        {
            echo "<$t>" . print_r($a, 1) . "</$t>";
        }
        $criteria = $this->searchCriteriaBuilder->create();
        $products = $this->productRepository->getList($criteria)->getItems();
        foreach($products as $product) {
            $productid = $product->getId();
            if($this->rentalHelper->isRentalType($product)) {
                $inventory = $this->stockManagement->updateFromTableSerializedArray($productid);
                $productname = $product->getName();
                echo "<h2>$productname</h2>";
                echo "<h3>Product Array From Reservations Orders Table Generated from Scratch and Updated</h3>";

                prettyPrint($inventory);
            }
        }
    }


}
