<?php

namespace SalesIgniter\Rental\Controller\InvUpdate;
use SalesIgniter\Rental\Cron\AsyncInventoryUpdate;



class UpdateInventory extends \Magento\Framework\App\Action\Action
{

    private $asyncInventoryUpdate;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        AsyncInventoryUpdate $asyncInventoryUpdate
    )
    {
        parent::__construct($context);
        $this->asyncInventoryUpdate = $asyncInventoryUpdate;
    }

    /**
     * site.com/salesigniter_rental/InvUpdate/UpdateInventory
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */

    public function execute()
    {
        $this->asyncInventoryUpdate->execute();
        print "Rental inventory updated";
        exit;
    }
}
