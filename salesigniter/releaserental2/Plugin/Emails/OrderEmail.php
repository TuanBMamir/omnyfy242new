<?php

namespace SalesIgniter\Rental\Plugin\Emails;

class OrderEmail
{
    public $state;

    public function __construct(
        \Magento\Framework\App\State $state
    ){
        $this->state = $state;
    }

    /**
     * @param \Magento\Sales\Block\Items\AbstractItems $subject
     * @param string $result
     *
     * @return string
     */
    public function afterToHtml(\Magento\Sales\Block\Items\AbstractItems $subject, $result)
    {
        /** @var Magento\Framework\App\State $state */
        //$this->state->emulateAreaCode(
           //'frontend',function() use ($subject, $result) {
            /** @var \SalesIgniter\Rental\Block\Emails\Serials\Serials $attributesBlock */
            if ($attributesBlock = $subject->getChildBlock('shipment_serials')) {
                $result .= $attributesBlock->toHtml();
            }
        //});
        return $result;
    }
}
