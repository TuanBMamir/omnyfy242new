<?php

namespace SalesIgniter\Rental\Plugin\Magento\Backend\Block\Widget\Context;


class AddSalesOrderButton
{

    private $request;

    private $urlBuilder;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\UrlInterface $urlBuilder,
        \SalesIgniter\Rental\Model\ResourceModel\ReservationOrders\CollectionFactory $collectionFactory
    ) {
        $this->request = $request;
        $this->urlBuilder = $urlBuilder;
        $this->collectionFactory = $collectionFactory;
    }

    public function afterGetButtonList(
        \Magento\Backend\Block\Widget\Context $subject,
        $buttonList
    ) {
        if ($subject->getRequest()->getFullActionName() === 'sales_order_view') {
        $orderid = $this->request->getParam('order_id');
            $returns = $this->collectionFactory->create();
            $returns->filterByToReturn()->filterByOrderId($orderid);
            $returns->load();
            $numberOfReturns = $returns->getSize();

        if($numberOfReturns > 0){
                $buttonList->add(
                    'return_order',
                    [
                        'label' => __('Return'),
                        'class' => 'returnItemsButton',
                    ]
                );
            }
        }

        return $buttonList;
    }
}
