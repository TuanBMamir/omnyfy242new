<?php

namespace SalesIgniter\Rental\Plugin\Magento\Catalog\Model\Product\Option\Type;

use Magento\Framework\Exception\LocalizedException;

class Date {

    private $rentalHelper;

    public function __construct(
        \SalesIgniter\Rental\Helper\Data $rentalHelper
    )
    {
        $this->rentalHelper = $rentalHelper;
    }

    public function aroundUseCalendar($subject, $proceed)
    {
        try {
            $product = $subject->getProduct();
            if($this->rentalHelper->isRentalType($product)){
                return false;
            } else {
                return $proceed();
            }
        } catch(LocalizedException $e) {
            return $proceed();
        }

    }


}
