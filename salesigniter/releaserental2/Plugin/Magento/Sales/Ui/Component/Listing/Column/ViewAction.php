<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace SalesIgniter\Rental\Plugin\Magento\Sales\Ui\Component\Listing\Column;

use Magento\Backend\Model\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Plugin that adds ship and return actions to sales order grid
 *
 * Class ViewAction
 */
class ViewAction extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;
    /**
     * @var \SalesIgniter\Rental\Model\ResourceModel\ReservationOrders\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        \SalesIgniter\Rental\Model\ResourceModel\ReservationOrders\CollectionFactory $collectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->collectionFactory = $collectionFactory;
        $this->orderRepository = $orderRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }


    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function afterPrepareDataSource(\Magento\Sales\Ui\Component\Listing\Column\ViewAction $subject, array $dataSource)
    {
        if (isset($dataSource['data']['items']) && $subject->getContext()->getNamespace() == 'sales_order_grid') {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['entity_id'])) {
                    $urlEntityParamName = $this->getData('config/urlEntityParamName') ?: 'entity_id';
                    if(isset($item['order_id'])) {
                        $shipitems = $this->getShipping($item['order_id']);
                        $returnitems = $this->getReturn($item['order_id']);
                        if ($shipitems >= 1) {
                            if($shipitems >= 2){
                                $shiptext = $shipitems . ' items to ship';
                            } else { $shiptext = $shipitems . ' item to ship'; }
                            $item['actions'] +=
                                [
                                    'Ship' => [
                                        'href' => $this->urlBuilder->getUrl('adminhtml/order_shipment/new',
                                            [
                                                'order_id' => $item['order_id']
//                                                '_secure' => true,
//                                                'key' => $this->urlBuilder->getSecretKey('admin','order_shipment', 'new')
                                            ]
                                        ),
                                        'label' => __($shiptext)
                                    ]];
                        }
                        if ($returnitems >= 1) {
                            if($returnitems >= 2){
                                $returntext = $returnitems . ' items to return';
                            } else { $returntext = $returnitems . ' item to return'; }
                            $item['actions'] +=
                                ['Return' => [
                                    'href' => $this->urlBuilder->getUrl( 'salesigniter_rental/returns/',
                                    [
                                    'order_id' => $item['order_id']
                                            ]
                                    ),
                                    'label' => __($returntext)
                                ]];
                        }
                    }
                }
            }
        }

        return $dataSource;
    }

    private function getShipping($orderid)
    {
        // Iterate through order items adding up getQtyToShip following method from vendor/magento/module-sales/Model/Order/ShipmentFactory.php
        $itemstoship = 0;
        $order = $this->orderRepository->get($orderid);
        foreach ($order->getAllItems() as $item) {
          $itemstoship += $item->getQtyToShip();
        }
        return $itemstoship;
    }

    private function getReturn($orderid)
    {
        $returns = $this->collectionFactory->create();
        $returns->filterByToReturn()->filterByOrderId($orderid);
        $returns->load();
        $numberOfReturns = $returns->getSize();
        return $numberOfReturns;
    }
}
