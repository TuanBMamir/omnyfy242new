/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'mage/translate',
    'mage/mage',
    'Magento_Catalog/product/view/validation',
    'catalogAddToCart'
], function ($,$t) {
    'use strict';

    $.widget('salesigniter.productValidate', {
        options: {
            bindSubmit: false,
            radioCheckboxClosest: '.nested',
            addToCartButtonSelector: '.action.tocart',
            buyoutButtonSelector: '.action.tocart.rental-buyout'
        },

        /**
         * Uses Magento's validation widget for the form object.
         * @private
         */
        _create: function () {
            var bindSubmit = this.options.bindSubmit;

            this.element.validation({
                radioCheckboxClosest: this.options.radioCheckboxClosest,

                /**
                 * Uses catalogAddToCart widget as submit handler.
                 * @param {Object} form
                 * @returns {Boolean}
                 */
                submitHandler: function (form) {
                    var jqForm = $(form).catalogAddToCart({
                        bindSubmit: bindSubmit
                    });

                    jqForm.catalogAddToCart('submitForm', jqForm);

                    return false;
                }
            });
            // only enable add to cart / rent button if not rental product, otherwise our pprdatepicker handles validation
            if($('div .sirent_calendar').length <= 0){
                $(this.options.addToCartButtonSelector).attr('disabled', false);
            }
            // buyout is ok to enable
            $(this.options.buyoutButtonSelector).attr('disabled', false);

        }
    });

    return $.salesigniter.productValidate;
});
