/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            'pprdatepicker': 'SalesIgniter_Rental/js/pprdatepicker',
            'spin': 'SalesIgniter_Rental/js/utils/spin',
            'css': 'SalesIgniter_Rental/js/utils/css',
            'ajaxq': 'SalesIgniter_Rental/js/utils/ajaxq',
            'maskspin': 'SalesIgniter_Rental/js/utils/jquery.maskspin',
            'jslogger': 'SalesIgniter_Rental/js/utils/jslogger',
            'pprtimepicker': 'SalesIgniter_Rental/js/utils/jquery.timepicker',
            'pricingppr': 'SalesIgniter_Rental/js/utils/jquery.pricingppr',
            'validateproduct': 'SalesIgniter_Rental/js/validate-product',
            'Magento_Catalog/js/catalog-add-to-cart': 'SalesIgniter_Rental/js/catalog-add-to-cart',
            momentrange: 'SalesIgniter_Rental/js/utils/moment-range'
        },
        'shim': {
            momentrange: {
                deps: ['moment']
            }
        }
    }
};
