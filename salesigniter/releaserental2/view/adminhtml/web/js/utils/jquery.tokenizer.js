/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/*jshint browser:true jquery:true */
/*eslint max-depth: 0*/

(function (factory) {
    'use strict';

    if (typeof define === 'function' && define.amd) {
        define([
            'jquery',
            'css!css/tokenize/bootstrap.min',
            'css!css/tokenize/tokenize2.min',
            'tokenize',
            'mage/validation',
            'mage/translate'
        ], factory);
    } else {
        factory(window.jQuery);
    }
}(function ($) {
    'use strict';

    $.widget('salesigniter.tokenizer', {
        options: {
            dataSource: '',
            tokensMaxItems: 0
        },
        _create: function () {
            this._initCalls();
        },
        _initCalls: function () {
            var self = this;
            this.element.tokenize2(this.options);
        }

    });

    $.validator.addMethod(
        'validate-serial', function (value, elem) {
            //
            let numOfSerials = $(elem).tokenize2().toArray().length;
            let inputQuantity = $(elem).prevAll('input').val();
            if (numOfSerials != inputQuantity) return false;
            return true;
        }, $.mage.__('Serial must be valid and number of serials must match quantity being sent'));

    return {
        tokenizer: $.salesigniter.tokenizer
    };
}));
