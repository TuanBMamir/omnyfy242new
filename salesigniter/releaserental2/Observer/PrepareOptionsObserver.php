<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace SalesIgniter\Rental\Observer;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\ObserverInterface;

class PrepareOptionsObserver implements ObserverInterface {

	/**
	 * @var \SalesIgniter\Rental\Helper\Data $_helperRental
	 */
	protected $_helperRental;
	/**
	 * @var \SalesIgniter\Rental\Helper\Calendar
	 */
	private $calendarHelper;
	/**
	 * @var \Magento\Catalog\Api\ProductRepositoryInterface
	 */
	private $productRepository;
	/**
	 * @var \Magento\Framework\App\RequestInterface
	 */
	private $request;

	/**
	 * @param \SalesIgniter\Rental\Helper\Data                $helperRental
	 * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
	 * @param \Magento\Framework\App\RequestInterface         $request
	 * @param \SalesIgniter\Rental\Helper\Calendar            $calendarHelper
	 */
	public function __construct(
		\SalesIgniter\Rental\Helper\Data $helperRental,
		ProductRepositoryInterface $productRepository,
		RequestInterface $request,
		\SalesIgniter\Rental\Helper\Calendar $calendarHelper
	) {
		$this->_helperRental     = $helperRental;
		$this->calendarHelper    = $calendarHelper;
		$this->productRepository = $productRepository;
		$this->request           = $request;
	}

	/**
     * Takes buy request, adds it to product options in quote_item_option will be like:
     * option_21 = 2020-05-20 23:59:00
     * option_22 = 2020-05-19 00:00
     *
	 * Function used to complete the custom options.
	 * Because Start date and Endate are custom options not required
	 * we hide them on product view/admin order create/etc but they
	 * need to be reconstructed before adding to cart from the form data posted.
     *
     * End result is to fill this data in info_buyRequest in quote_item_option table
     * Magento has this for default datetime pickers but since we use custom date time picker
     * we need to fill this in manually from calendar_selector form data
    [10] => stdClass Object
    (
    [month] => 12
    [day] => 27
    [year] => 2019
    [hour] => 12
    [minute] => 00
    [day_part] => am
    [date_internal] => 2019-12-27 00:00:00
    )
	 *
	 * @param \Magento\Framework\Event\Observer $observer
	 *
	 * @return $this
	 * @throws \Magento\Framework\Exception\LocalizedException
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
	 * @SuppressWarnings(PHPMD.NPathComplexity)
	 */
	public function execute( \Magento\Framework\Event\Observer $observer ) {
		/* @var $product \Magento\Catalog\Model\Product */
		$product    = $this->productRepository->getById( $observer->getEvent()->getProduct()->getId() );
		$buyRequest = $observer->getEvent()->getBuyRequest();
		$transport  = $observer->getEvent()->getTransport();
		/** @var array $calendarSelector */
		$calendarSelector = $buyRequest->getCalendarSelector();

		// Build $optionsArr from product options, then set it to buy request options
		if ( $this->_helperRental->isRentalType( $product ) ) {
			if ( $calendarSelector === null && $this->request->getParam( 'related_product' ) ) {
				$calendarSelector = $this->request->getParam( 'calendar_selector' );
			}
			$optionsArr  = [];
			$optionsInit = $buyRequest->getOptions();
			foreach ( $product->getOptions() as $option ) {
				if ( is_object( $option ) ) {
					if ( isset( $optionsInit[ $option->getId() ] ) ) {
						$optionsArr[ $option->getId() ] = $optionsInit[ $option->getId() ];
					} else {
						$optionsArr[ $option->getId() ] = '';
					}
				}
			}
			$buyRequest->setOptions( $optionsArr );
		}


		if ( $calendarSelector !== null && $this->_helperRental->isRentalType( $product ) ) {
			$options = $buyRequest->getOptions();

			$hasTimes = $this->calendarHelper->useTimes( $product );
			if ( $buyRequest->getCalendarUseTimes() ) {
				$hasTimes = $buyRequest->getCalendarUseTimes() === '1';
			}
			try {
				/** @var \DateTime $startDate */
				$startDate = $this->calendarHelper->convertDateToUTC( $calendarSelector['from'], $hasTimes, $calendarSelector['locale'] );
				/** @var \DateTime $endDate */
				$endDate = $this->calendarHelper->convertDateToUTC( $calendarSelector['to'], $hasTimes, $calendarSelector['locale'] );
				if ( ! $hasTimes && $this->calendarHelper->getHotelMode( $product ) === 0 ) {
				    // Set end time to 11:59pm when not using hotel mode or time of day so whole day is booked
					$endDate = $endDate->add( new \DateInterval( 'PT23H59M' ) );
				}
                if ( ! $hasTimes && $this->calendarHelper->getHotelMode( $product ) == 1 ) {
                    // Set start time to 12:01pm when using hotel mode so end date can be on start date
                    $startDate = $startDate->add( new \DateInterval( 'PT12H1M' ) );
                }
				/** @var array $optionsTemp */
				$optionsTemp = [];

				// Set product option start and end date to the same as calendar_selector but in the array format Magento uses
				if ( is_array( $options ) && ( $calendarSelector !== null || $buyRequest->getIsBuyout() ) ) {
					foreach ( $options as $optionId => $optionData ) {
						$option = $product->getOptionById( $optionId );
						if ( is_object( $option ) ) {
							if ( ! $buyRequest->getIsBuyout() ) {
								if ( $this->_helperRental->optionContainsLocaleTranslations($option->getTitle(), 'start')) {
									$optionData = $this->setOptionData( $startDate, $optionData );
								}
								if ( $this->_helperRental->optionContainsLocaleTranslations($option->getTitle(), 'end')) {
									$optionData = $this->setOptionData( $endDate, $optionData );
								}
							} else {
								if ( $this->_helperRental->optionContainsLocaleTranslations($option->getTitle(), 'buyout')) {
									$optionData = 'Rental Buyout';
								}
							}
						}

						if ( isset( $optionsInit[ $optionId ] ) ) {
							$optionsTemp[ $optionId ] = $optionData;
						}
					}


					if ( count( $optionsTemp ) > 0 ) {
						$buyRequest->setOptions( $optionsTemp );
						foreach ( $product->getOptions() as $option ) {
							/* @var $option \Magento\Catalog\Model\Product\Option */
							$group = $option->groupFactory( $option->getType() )
							                ->setOption( $option )
							                ->setProduct( $product )
							                ->setRequest( $buyRequest )
							                ->setProcessMode( 'full' )
							                ->validateUserValue( $buyRequest->getOptions());

							$preparedValue = $group->prepareForCart();
							if ( $preparedValue !== null ) {
								$transport->options[ $option->getId() ] = $preparedValue;
							}
						}
					}
				}
			} catch ( Exception $e ) {
			}
		}

		return $this;
	}

	/**
	 * @param \DateTime $calendarSelectorDate
	 * @param array     $optionData
	 *
	 * @return mixed
	 */
	private function setOptionData($calendarSelectorDate, $optionData ) {
	    if(!is_array($optionData)){
	        $optionData = [];
        }
		if ( is_array( $optionData ) ) {
			$optionData['day']           = $calendarSelectorDate->format( 'd' );
			$optionData['month']         = $calendarSelectorDate->format( 'm' );
			$optionData['year']          = $calendarSelectorDate->format( 'Y' );
			$optionData['minute']        = $calendarSelectorDate->format( 'i' );
			$optionData['date_internal'] = $calendarSelectorDate->format( 'Y-m-d H:i:s' );
			if ( $this->calendarHelper->timeTypeAmpm() ) {
				$optionData['hour']     = $calendarSelectorDate->format( 'h' );
				$optionData['day_part'] = $calendarSelectorDate->format( 'a' );

				return $optionData;
			} else {
				$optionData['hour'] = $calendarSelectorDate->format( 'H' );

				return $optionData;
			}
		}
	}
}
