<?php

namespace SalesIgniter\Rental\Observer;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use \Magento\Framework\ObjectManagerInterface;

/**
 * Class ShipmentSaveCommited
 *
 * @package SalesIgniter\Rental\Observer
 */
class ShipmentSaveCommitted implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * Injected Dependency Description
     *
     * @var \\SalesIgniter\Rental\Model\ShipmentProcessor
     */
    protected $rentalModelShipmentProcessor;

    /**
     * Injected Dependency Description
     *
     * @var \SalesIgniter\Rental\Api\ReservationOrdersRepositoryInterface
     */
    protected $apiReservationOrdersRepositoryInterface;

    /**
     * @var \SalesIgniter\Rental\Helper\Data
     */
    private $helperRental;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;
    /**
     * @var \SalesIgniter\Rental\Api\StockManagementInterface
     */
    private $stockManagement;
    /**
     * @var \Magento\Sales\Api\OrderItemRepositoryInterface
     */
    private $orderItemRepository;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * ShipmentSaveBefore constructor.
     *
     * @param \SalesIgniter\Rental\Helper\Data                              $helperRental
     * @param \Magento\Framework\App\RequestInterface                       $request
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                  $searchCriteriaBuilder
     * @param \Magento\Sales\Api\OrderItemRepositoryInterface               $orderItemRepository
     * @param \SalesIgniter\Rental\Api\ReservationOrdersRepositoryInterface $apiReservationOrdersRepositoryInterface
     * @param \SalesIgniter\Rental\Api\StockManagementInterface             $stockManagement
     * @param \SalesIgniter\Rental\Model\ShipmentProcessor                  $rentalModelShipmentProcessor
     */
    public function __construct(
        \SalesIgniter\Rental\Helper\Data $helperRental,
        RequestInterface $request,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderItemRepositoryInterface $orderItemRepository,
        \SalesIgniter\Rental\Api\ReservationOrdersRepositoryInterface $apiReservationOrdersRepositoryInterface,
        \SalesIgniter\Rental\Api\StockManagementInterface $stockManagement,
        \SalesIgniter\Rental\Model\ShipmentProcessor $rentalModelShipmentProcessor,
        ObjectManagerInterface $objectManager
)
    {
        $this->rentalModelShipmentProcessor = $rentalModelShipmentProcessor;
        $this->apiReservationOrdersRepositoryInterface = $apiReservationOrdersRepositoryInterface;
        $this->helperRental = $helperRental;
        $this->request = $request;
        $this->stockManagement = $stockManagement;
        $this->orderItemRepository = $orderItemRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->objectManager = $objectManager;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /* @var \Magento\Sales\Model\Order\Shipment $shipment */
        $shipment = $observer->getEvent()->getShipment();
        if(method_exists($shipment->getExtensionAttributes(), 'getSourceCode')){
            $sourceCode = $shipment->getExtensionAttributes()->getSourceCode();
        } else {
            $sourceCode = null;
        }


        /** @var array $postedData */
        $postedData = $this->request->getParams();

        if (array_key_exists('shipment', $postedData) && array_key_exists('items', $postedData['shipment'])) {
            foreach ($shipment->getAllItems() as $item) {
                $orderItemId = $item->getOrderItemId();
                $qty = $item->getQty();
                $orderid = $shipment->getOrderId();
                $serialsToShip = [];
                $storeid = $shipment->getStoreId();
                if (array_key_exists('serial', $postedData) && array_key_exists($orderItemId, $postedData['serial'])) {
                    // if coming from admin ship order page $postedData['serial'][$orderItemId] is array
                    if(is_array($postedData['serial'][$orderItemId])){
                        $serialsToShip = $postedData['serial'][$orderItemId];
                    } else {
                        // if coming from send rentals grid, $postedData['serial'][$orderItemId] is string
                        $serialsToShip = explode(',', $postedData['serial'][$orderItemId]);
                    }

                }
                $qtyToShip = $qty;
                $productId = $item->getProductId();

                $reservationOrder = $this->apiReservationOrdersRepositoryInterface->getByOrderItemId($orderItemId, $sourceCode);

                // MSI adjusts reservation orders in case shipment quantities are different than what was already reserved
                if($this->helperRental->isRentalInventoryEnabled())
                {
                    $stockManager = $this->objectManager->get('SalesIgniter\Rentalinventory\Model\StockManagement');

                    // if no reservation order yet exists for that source code, set it to newly created one and adjust reservation orders according to shipment
                    if($reservationOrder == null){
                        $reservationOrder = $stockManager->adjustReservationsordersAccordingToShipmentRequest($productId, $orderid, $qtyToShip, $sourceCode, $storeid);
                    } else {
                        $stockManager->adjustReservationsordersAccordingToShipmentRequest($productId, $orderid, $qtyToShip, $sourceCode, $storeid);
                    }

                }

                // Order item is a reservation product
                if ($reservationOrder !== null) {
                    $this->updateQuantityAndSerialsAndShip($reservationOrder, $qtyToShip, $serialsToShip, $shipment);

                    // Order item is a configurable or bundle get the parent reservation products
                } else {
                    $items = $this->getReservationorderParentItems($orderItemId);
                    /** @var \Magento\Sales\Api\Data\OrderItemInterface $orderItem */
                    foreach ($items as $orderItem) {
                        $reservationOrder = $this->apiReservationOrdersRepositoryInterface->getByOrderItemId($orderItem->getItemId(), $sourceCode);
                        $qtyToShip = $qty * $orderItem->getQtyOrdered(); /*- $orderItem->getQtyShipped();*/
                        if ($reservationOrder !== null) {
                            $this->updateQuantityAndSerialsAndShip($reservationOrder, $qtyToShip, $serialsToShip, $shipment);
                        }
                    }
                }
            }
        }
    }

    public function getReservationorderParentItems($orderItemId)
    {
    $this->searchCriteriaBuilder->addFilter('parent_item_id', $orderItemId);
                    $criteria = $this->searchCriteriaBuilder->create();
                    $items = $this->orderItemRepository->getList($criteria)->getItems();
                    return $items;
    }

    public function updateQuantityAndSerialsAndShip($reservationOrder, $qtyToShip, $serialsToShip, $shipment)
    {
        $reservationSerialsToShip = $this->makeSureReservationSerialsToShipIsArray($serialsToShip);
            $this->rentalModelShipmentProcessor->fixQtyToShipToMatchSerialsChosentoShip($reservationOrder, $qtyToShip, $reservationSerialsToShip);
            if ($qtyToShip > 0) {
                $this->stockManagement->shipReservation($reservationOrder, $qtyToShip, $reservationSerialsToShip, $shipment);
            }
    }

    /**
     * Fixes serials to ship to be an array
     *
     * @param $serialsToShip
     * @return array
     */
    public function makeSureReservationSerialsToShipIsArray($serialsToShip){
        if(is_array($serialsToShip)){
            $reservationSerialsToShip = $serialsToShip;
        }

        if(is_string($serialsToShip)){
            $reservationSerialsToShip = explode(',', $serialsToShip);
        }

        if(isset($reservationSerialsToShip)) {
            if (is_array($reservationSerialsToShip) === false) {
                $qtyToShipFromSerials = [$reservationSerialsToShip];
            }
        }

        if ($reservationSerialsToShip == '' || $reservationSerialsToShip == null){
            $reservationSerialsToShip = [];
        }
        return $reservationSerialsToShip;
    }
}
