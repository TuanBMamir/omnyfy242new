<?php

namespace SalesIgniter\Rental\Ui\DataProvider\Reservation;


use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Data\Collection;
use Magento\Ui\DataProvider\AddFilterToCollectionInterface;

/**
 * Class YourCustomFilterStrategy
 */
class ProductSkuCustomFilterStrategy implements AddFilterToCollectionInterface
{
    /**
     * @param Collection $collection
     * @param string $field
     * @param null $condition
     */
    public function addFilter(Collection $collection, $field, $condition = null)
    {
        $collection->getSelect()->where(
            'at_sku_default.value' . ' = ' . '\'' . $condition['eq'] . '\''
        );
    }
}
