<?php
namespace SalesIgniter\Rental\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\Component\Form;
use Magento\Framework\Stdlib\ArrayManager;

class SingleDay extends AbstractModifier
{
    protected $locator;

    protected $arrayManager;


    public function __construct(
        \SalesIgniter\Rental\Helper\Data $rentalHelper,
        LocatorInterface $locator,
        ArrayManager $arrayManager
    ) {
        $this->rentalHelper = $rentalHelper;
        $this->locator = $locator;
        $this->arrayManager = $arrayManager;
    }

    public function modifyData(array $data)
    {
        $modelId = $this->locator->getProduct()->getId();

        $attrs = [
            'sirent_single_day_mode',
        ];

        foreach ($attrs as $attr) {
            if (!isset($data[$modelId][static::DATA_SOURCE_DEFAULT][$attr])) {
                $data[$modelId][static::DATA_SOURCE_DEFAULT]['use_config_' . $attr] = '1';
            }
        }

        return $data;
    }

    public function modifyMeta(array $meta)
    {
        $meta = $this->customizeMeta($meta, 'sirent_single_day_mode');

        return $meta;
    }

    protected function customizeMeta(array $meta, $code)
    {
        $path = $this->arrayManager->findPath($code, $meta, null, 'children');

        if ($path) {

            $meta = $this->arrayManager->merge(
                $path . static::META_CONFIG_PATH,
                $meta,
                [
                    'tooltip' => [
                        'description' => __(
                            'Single day for every rental'
                        ),
                    ],
                    'imports' => [
                        'disabled' =>
                            '${$.parentName}.use_config_'
                            . $code
                            . ':checked',
                        '__disableTmpl' => ['disabled' => false],
                    ],
                    'validation' => [
                        'validate-no-empty' => true
                    ],
                ]
            );

            $containerPath = $this->arrayManager->findPath(
                static::CONTAINER_PREFIX . $code,
                $meta,
                null,
                'children'
            );

            $meta = $this->arrayManager->merge($containerPath . static::META_CONFIG_PATH, $meta, [
                'component' => 'Magento_Ui/js/form/components/group',
            ]);

            $meta = $this->arrayManager->merge(
                $containerPath,
                $meta,
                [
                    'children' => [
                        'use_config_' . $code => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'dataType' => 'number',
                                        'formElement' => Form\Element\Checkbox::NAME,
                                        'componentType' => Form\Field::NAME,
                                        'description' => __('Use Config Settings'),
                                        'dataScope' => 'use_config_' . $code,
                                        'valueMap' => [
                                            'false' => '0',
                                            'true' => '1',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ]
                ]
            );
        }

        return $meta;
    }
}
