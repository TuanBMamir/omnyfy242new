<?php

namespace SalesIgniter\Rental\Ui\Component\Listing\DataProviders\SalesIgniter\ManualEdit;

class Form extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;
    /**
     *
     */
    private $resOrderId;
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderrepo;

    /**
     * Grid constructor.
     *
     * @param string                                                                       $name
     * @param string                                                                       $primaryFieldName
     * @param string                                                                       $requestFieldName
     * @param \SalesIgniter\Rental\Model\ResourceModel\ReservationOrders\CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\RequestInterface                                      $request
     * @param array                                                                        $meta
     * @param array                                                                        $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \SalesIgniter\Rental\Model\ResourceModel\ReservationOrders\CollectionFactory $collectionFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->orderrepo = $orderRepository;
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection();
            $this->getCollection()->load();
        }

        $arrItems = [
            'totalRecords' => $this->getCollection()->getSize(),
            'items' => [],
        ];

        foreach ($this->getCollection() as $item) {
            $itemAsArray = $item->toArray([]);
            //$itemAsArray['id_field_name'] = 'reservationorder_id';
            $itemAsArray['is_shipped'] = 0;
            $itemAsArray['reservation_id'] = $itemAsArray['reservationorder_id'];
            if ($itemAsArray['qty_shipped'] > 0) {
                $itemAsArray['is_shipped'] = 1;
            }
            $itemAsArray['storeid'] = 0;
            if($itemAsArray['order_id'] != 0){
                $itemAsArray['storeid'] = $this->orderrepo->get($itemAsArray['order_id'])->getStoreId();
            }

            $arrItems[$itemAsArray['reservationorder_id']] = $itemAsArray;
        }

        return $arrItems;
    }
}
