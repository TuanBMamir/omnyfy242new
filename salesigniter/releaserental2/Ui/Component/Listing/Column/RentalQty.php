<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace SalesIgniter\Rental\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Backend\Model\UrlInterface;

class RentalQty extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * Column name
     */
    const NAME = 'column.qty_available';

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $localeCurrency;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \SalesIgniter\Rental\Model\Product\Stock
     */
    private $helperStock;
    /**
     * @var \SalesIgniter\Rental\Helper\Data
     */
    private $helperRental;
    /**
     * @var \SalesIgniter\Rental\Api\StockManagementInterface
     */
    private $stockManagement;

    private $urlBuilder;

    /**
     * @param ContextInterface                                  $context
     * @param UiComponentFactory                                $uiComponentFactory
     * @param \Magento\Framework\Locale\CurrencyInterface       $localeCurrency
     * @param \Magento\Store\Model\StoreManagerInterface        $storeManager
     * @param \SalesIgniter\Rental\Model\Product\Stock          $helperStock
     * @param \SalesIgniter\Rental\Api\StockManagementInterface $stockManagement
     * @param \SalesIgniter\Rental\Helper\Data                  $helperRental
     * @param array                                             $components
     * @param array                                             $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \SalesIgniter\Rental\Model\Product\Stock $helperStock,
        \SalesIgniter\Rental\Api\StockManagementInterface $stockManagement,
        \SalesIgniter\Rental\Helper\Data $helperRental,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->localeCurrency = $localeCurrency;
        $this->storeManager = $storeManager;
        $this->helperStock = $helperStock;
        $this->helperRental = $helperRental;
        $this->stockManagement = $stockManagement;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function prepareDataSource(array $dataSource)
    {
        $startDateArr = $this->context->getFilterParam('start_date');
        if(is_array($startDateArr)){
        $startDate = $startDateArr['from'];
        $endDate = $startDateArr['to'];
        } else {
            $startDate = null;
            $endDate = null;
        }

        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                if ($this->helperRental->isRentalType($item['entity_id']) && !$this->helperRental->isBundle($item['entity_id'])) {
                    $product = $this->helperRental->getProductObjectFromId($item['entity_id']);
                    $sku = $product->getSku();
                    $urllink = $this->urlBuilder->getUrl('salesigniter_rental/report/inventory',[
                        'filtertext' => $sku,
                            'filterby' => 'sku',
                            'filtertype' => 'exact'
                        ]
                        );
                    $reslink = $this->urlBuilder->getUrl('salesigniter_rental/manualedit/index',[
                            'sku' => $sku,
                        ]
                    );
                    $return = '';
                    if($this->helperRental->isRentalInventoryEnabled() == false){
                        $available = $this->stockManagement->getAvailableQuantity($product, $startDate, $endDate);
                        $total = $this->stockManagement->getSirentQuantity($product);
                        $booked = $total - $available;
                        $return .= '<b>Total:</b> ' . $total . '<br />';
                        $return .= '<b>Booked:</b> ' . $booked . '<br />';
                        $return .= '<b>Available:</b> ' . $available . '<br />';
                    }
                    $return .= '<a href="' . $urllink . '" target="_blank">' . __('Inv Report') . '</a><br />';
                    $return .= '<a href="' . $reslink . '" target="_blank">' . __('Res History') . '</a>';
                    $item[$fieldName] = $return;
                }
            }
        }

        return $dataSource;
    }
}
