<?php
namespace SalesIgniter\Rental\Block\Product\View\Options;

use Magento\Catalog\Model\Product;

class Singleday extends \Magento\Framework\View\Element\Template
{
    protected $_product = null;

    protected $_coreRegistry = null;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \SalesIgniter\Rental\Helper\Calendar $calendarHelper,                        
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_calendarHelper = $calendarHelper;        
        parent::__construct($context, $data);
    }

    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }
    
    public function getSingleDayMode()
    {
        return $this->_calendarHelper->getSingleDayMode($this->getProduct());       
    }    
}

