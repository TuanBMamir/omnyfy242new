<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Orderattr
 */


namespace SalesIgniter\Rental\Block\Emails\Serials;


use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use SalesIgniter\Rental\Api\ReservationOrdersRepositoryInterface;

class Serials extends Template
{
    /**
     * @var string[]
     */
    private $escapedInputTypes = ['file', 'html'];

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ReservationOrdersRepositoryInterface
     */
    private $reservationOrdersRepository;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $product;


    public function __construct(
        Template\Context $context,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Registry $coreRegistry,
        ReservationOrdersRepositoryInterface $reservationOrdersRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $product,
        array $escapedInputTypes = [],
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->product = $product;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->reservationOrdersRepository = $reservationOrdersRepository;
        $this->coreRegistry = $coreRegistry;
        $this->escapedInputTypes = array_merge($this->escapedInputTypes, $escapedInputTypes);
    }

   public function getProductNameAndAssignedSerialsArray(){
        $productSerialsArray = [];
       $this->searchCriteriaBuilder->addFilter('sales_shipment_id', $this->getData('shipment_id'));
       $this->searchCriteriaBuilder->addFilter('ship_date', new \Zend_Db_Expr('not null'), 'is');
       $this->searchCriteriaBuilder->addFilter('serials_shipped', new \Zend_Db_Expr('not null'), 'is');
       $criteria = $this->searchCriteriaBuilder->create();
       $items = $this->reservationOrdersRepository->getList($criteria)->getItems();

       foreach($items as $item){
           $productname = $this->product->getById($item['product_id'])->getName();
           $serials = explode(',',$item['serials_shipped']);
           $productSerialsArray[$productname] = $serials;
       }

       return $productSerialsArray;
   }
}
