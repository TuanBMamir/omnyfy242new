<?php

namespace SalesIgniter\Rental\Block\Adminhtml\Widget\DataTable\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\Text;
use Magento\Framework\DataObject;

class OrderIncrement extends Text
{

    /**
     * @param DataObject $row
     *
     * @return string
     */
    public function _getValue(DataObject $row)
    {
        $RenderConfig = $this->getColumn()->getData('renderConfig');
        $Template = $RenderConfig['template'];

        if ($row->getData('order_type') == 'rfq') {
            $orderOrRFQLink = '<a href="' . $this->getUrl('amasty_quote/quote/view', ['quote_id' => $row->getData('order_id')]) . '" target="_blank">RFQ #' . $row->getData('order_increment_id') . '</a>';
        } else if ($row->getData('order_type') == 'order') {
            $orderOrRFQLink = '<a href="' . $this->getUrl('sales/order/view', ['order_id' => $row->getData('order_id')]) . '" target="_blank">' . $row->getData('order_increment_id') . '</a>';
        } else if ($row->getData('order_type') == 'manual') {
            $orderOrRFQLink = 'Manual Reservation';//'<a href="' . $this->getUrl('salesigniter_rental/manualedit/edit', ['order_id' => $row->getData('order_id')]) . '" target="_blank">' . $row->getData('order_increment_id') . '</a>';
        } else if ($row->getData('order_type') == 'maintenance') {
            $orderOrRFQLink = 'Maintenance Ticket';//'<a href="' . $this->getUrl('salesigniter_maintenance/ticket/edit', ['order_id' => $row->getData('order_id')]) . '" target="_blank">' . $row->getData('order_increment_id') . '</a>';
        }

        return $orderOrRFQLink;
    }
}
