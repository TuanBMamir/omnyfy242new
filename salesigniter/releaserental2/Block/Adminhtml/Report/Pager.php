<?php

namespace SalesIgniter\Rental\Block\Adminhtml\Report;

use \Magento\Framework\ObjectManagerInterface;
use \SalesIgniter\Rental\Helper\Data;
use \SalesIgniter\Rental\Block\Adminhtml\Report\Inventory\Chooser;

class Pager extends \Magento\Theme\Block\Html\Pager
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        ObjectManagerInterface $objectManager,
        Data $rentalHelper,
        \SalesIgniter\Rental\Block\Adminhtml\Report\Inventory\Chooser $chooser,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->objectManager = $objectManager;
        $this->rentalHelper = $rentalHelper;
        $this->chooser = $chooser;
    }

    public function getCategoryChooserHtml()
    {
        return $this->chooser->getElementHtml();
    }

    public function getDisplayBy()
    {
        $displayby = $this->getRequest()->getParam('displayby');
        $output = '<div id="sourorstockdiv" style="display:inline">' . __('Display By:') . ' <select name="sourceorstock" id="sourceorstock">';
        $output .= '<option value="source"';
        if($displayby == 'source'){
            $output .= ' selected="selected"';
        }
        $output .= '>Source</option><option value="stock"';
        if($displayby == 'stock'){
            $output .= ' selected="selected"';
        }
        $output .= '>Stock</option></select></div>';
        return $output;
    }

    public function getSources()
    {
        $sources = $this->getRequest()->getParam('sources');
        $stockManager = $this->objectManager->create('SalesIgniter\Rentalinventory\Model\StockManagement');
        $sourceItems = $stockManager->getSources();
        $output = '<div id="sourcesdiv" style="display:inline">' . __('Sources:') . ' <select name="sources" id="sources">';
        $all = '';
        if(isset($sources) && $sources == 'all'){ $all = 'selected="selected"';}
        $output .= '<option value="all" ' . $all . '>All</option>';
        foreach($sourceItems as $source){
            $selected = '';
            if(isset($sources) && $source->getSourceCode() == $sources){ $selected = 'selected="selected"';}
           $output .= '<option value="' . $source->getSourceCode() . '" ' . $selected . '>'. $source->getName() . '</option>';
        }
        $output .= '</select></div>';
        return $output;
    }

    public function getStocks()
    {
        $stocks = $this->getRequest()->getParam('stocks');
        $stockManager = $this->objectManager->create('SalesIgniter\Rentalinventory\Model\StockManagement');
        $stockItems = $stockManager->getStocks();
        $output = '<div id="stocksdiv" style="display:inline">' . __('Stocks:') . ' <select name="stocks" id="stocks">';
        $all = '';
        if(isset($stocks) && $stocks == 'all'){ $all = 'selected="selected"';}
        $output .= '<option value="all" ' . $all . '>All</option>';
        foreach($stockItems as $stock){
            $selected = '';
            if(isset($stocks) && $stock->getStockId() == $stocks){ $selected = 'selected="selected"';}
            $output .= '<option value="' . $stock->getStockId() . '" ' . $selected . '>'. $stock->getName() . '</option>';
        }
        $output .= '</select></div>';
        return $output;
    }


}
