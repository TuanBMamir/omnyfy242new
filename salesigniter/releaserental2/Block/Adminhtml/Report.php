<?php

namespace SalesIgniter\Rental\Block\Adminhtml;

use Magento\Backend\Block\Template as BlockTemplate;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Json\EncoderInterface;

class Report extends BlockTemplate
{

    /**
     * @var EncoderInterface
     */
    protected $_jsonEncoder;

    /**
     * Report constructor.
     *
     * @param Context          $context
     * @param EncoderInterface $JsonEncoder
     * @param array            $data
     */
    public function __construct(
        Context $context,
        EncoderInterface $JsonEncoder,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_jsonEncoder = $JsonEncoder;
    }

    /**
     * @param $Code
     *
     * @return $this
     */
    public function setCode($Code)
    {
        $this->setData('code', $Code);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->getData('code');
    }

    /**
     * @return string
     */
    public function getReportConfig()
    {
        $filterparams = [];
        // Add extra filters from Main Url here (not ajax url) like report/inventory url
            $filterparams = [
                'filtertext' => $this->getRequest()->getParam('filtertext'),
                'filterby' => $this->getRequest()->getParam('filterby'),
                'filtertype' => $this->getRequest()->getParam('filtertype'),
                'category_ids' => $this->getRequest()->getParam('category_ids'),
                'displayby' => $this->getRequest()->getParam('displayby'),
                'stocks' => $this->getRequest()->getParam('stocks'),
                'sources' => $this->getRequest()->getParam('sources'),
            ];
        $pagelimit = $this->getRequest()->getParam('limit');
        if($pagelimit == null){
            $pagelimit = 20;
        }
        $Configuration = [
            'code' => $this->getCode(),
            'Report' => [
                'dataUrl' => $this->getUrl('*/*/getReportData'),
            ],
            'Filter' =>  $filterparams
            ,
            'Pager' => [
                'pageVarName' => 'p',
                'page' => 1,
                'limitVarName' => 'limit',
                'limit' => $pagelimit
            ],
            'Calendar' => [
                'rendererCode' => 'month'
            ]
        ];

        return $this->_jsonEncoder->encode($Configuration);
    }
}
