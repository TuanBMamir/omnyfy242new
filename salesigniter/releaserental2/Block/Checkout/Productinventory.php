<?php
namespace SalesIgniter\Rental\Block\Checkout;

use Magento\Catalog\Model\Product;
use Magento\Framework\Url as UrlBuilder;

class Productinventory extends \Magento\Framework\View\Element\Template
{
    protected $_product = null;

    protected $_coreRegistry = null;

    private $urlBuilder;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \SalesIgniter\Rental\Helper\Calendar $calendarHelper,
        UrlBuilder $urlBuilder,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_calendarHelper = $calendarHelper;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $data);
    }

    /**
     * Get URL for ajax price call.
     *
     * @return string
     */
    public function getInventoryUpdateUrl()
    {

        return $this->urlBuilder->getUrl(
            'salesigniter_rental/ajax/updateinventory',
            [
                '_secure' => $this->getRequest()->isSecure()
            ]
        );
    }


}

